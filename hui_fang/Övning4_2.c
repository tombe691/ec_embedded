#include<stdio.h>

int main(){
	int c;	//Declare variable.
	
	// Display requirements.
	printf("Number 1 to 12 and"
	"their square and cubic are displayed: \n");

	//Use the for loop to print result.
	for (c=1; c<13; c++ ){
		printf("%10d %10d %10d \n", c, c*c, c*c*c);
	}

	return 0;
}