#include <iostream>

using namespace std;

class Book {
    public:
        string title;
        string author;
        int pageNum;
        float price;

    void print(){
        cout << "Title of the book: " << title <<"\n";
        cout << "Author of the book: " << author<<"\n";
        cout << "Total pages of the book: " << pageNum<<"\n";
        cout << "Price of the book: " << price<< " kr." <<"\n\n";
    }
};



int main(){
    Book book1;
    Book book2;

    book1={"C from Beginning", "Jan Skansholm", 2016, 278.5};
    book2={"C plus plus", "Hans Borman", 325, 375};


    book1.print();

    cout << "======================================\n\n";

    book2.print();

    return 0;
}
