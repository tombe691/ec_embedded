/*

File: prog_4.2.c

Created by Tony Anderstedt
Date: 2019-10-01

Programme thats loops and counts!

*/

#include <stdio.h>
#include <math.h>

int main () {
	
	// Start loop from 1 to 12
	for (int i = 1; i < 13; i++) {
		
		// Show the view the math magic!
		printf("%d, %d, %d\n", i, i*i, i*i*i);
		
	}
	
	//End of programme
	return 0;
}