#include "Arduino.h"

class Display {
private:
  // Defines how the different leds are connected to the boards output
  int pin_connections[8];
  // 2d array defining how to print numbers
  int numbers[12][8];
  // High/low valuse. Defines common cathode or common anode
  int h = 1, l = 0;
  // Defines how the different leds are connected to the boards output
  int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, dp = 8;

public:
  // Constructors
  Display() { init(); };
  Display(int a, int b, int c, int d, int e, int f, int g, int dp);
  // Methods
  void setPins(int a, int b, int c, int d, int e, int f, int g, int dp);
  void init();
  void commonCatode();
  void commonAnode();
  void print(int num);
  void dot();
  void clear();
};