#include <iostream>
#include <cmath>

using namespace std;

bool string_to_double(string text, double *number);

int main() {
    while (1) {
        string input;
        double n1, n2;

        cout << "\nEnter q to quit\nOtherwise enter a number.\n" << endl;
        while (1) {
//            Get input
            cin >> input;
            cin.clear();
            cin.ignore(INT_MAX, '\n');

            if (tolower(input[0]) == 'q') {
//                Exit program
                return 0;
            }
//            Convert string to double
            if (string_to_double(input, &n1)) {
//                Exit loop
                break;
            }
            cout << "Invalid input!\nPlease enter a number or q: ";
        }

        cout << "Enter another number: " << endl;
        while (1) {
//            Get input
            cin >> input;
            cin.clear();
            cin.ignore(INT_MAX, '\n');

            if (tolower(input[0]) == 'q') {
//                Exit program
                return 0;
            }
//            Convert string to double
            if (string_to_double(input, &n2)) {
//                Exit loop
                break;
            }
            cout << "Invalid input!\nPlease enter a number or q: ";
        }

//        Print results
        cout << "The positive difference between " << n1 << " and " << n2 << " is " << fdim(n1, n2) << endl;
        cout << "The cubic root of " << n2 << " is " << cbrt(n2) << endl;
        cout << "The hypotenuse of a right-angled triangle is " << hypot(n1, n2) <<
             " if the legs are " << n1 << " and " << n2 << endl;
    }
}

bool string_to_double(string text, double *number) {
    for (int i = 0; i < text.length(); ++i) {
        if (!isdigit(text[i])) {
            if (text[i] == '.'){
                continue;
            }
            return false;
        }
    }
    *number = stod(text);
    return true;
}
