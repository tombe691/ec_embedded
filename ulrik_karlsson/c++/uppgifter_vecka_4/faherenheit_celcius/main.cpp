#include <iostream>
#include <cctype>

using namespace std;

double fahrenheit_to_celsius(double fahrenheit);


int main() {
    while (1) {
        char input;
        double input_fahrenheit, celsius;

        cout << "Enter temperature in Fahrenheit: ";
//        Get input from user
        cin >> input_fahrenheit;
//        Make sure input is valid
        while (cin.fail()) {
//            Clear the input stream
            cin.clear();
//            Ignore remaining input
            cin.ignore(INT_MAX, '\n');
            cout << "Invalid input! Please enter a number: ";
            cin >> input_fahrenheit;
        }
//        Ignore remaining input
        cin.ignore(INT_MAX, '\n');

//        Convert and print result
        celsius = fahrenheit_to_celsius(input_fahrenheit);
        cout << endl << input_fahrenheit << " Fahrenheit is " << celsius << " Celsius. \n";

        cout << "Do you want to continue? y/n: ";
//        Get input from user and convert it to lowercase
        cin >> input;
        char lowercase_input;
        lowercase_input = tolower(input);
//        Make sure input is valid
        while (!(lowercase_input == 'y' || lowercase_input == 'n')) {
//            Clear the input stream
            cin.clear();
//            Ignore remaining input
            cin.ignore(INT_MAX, '\n');
            cout << "Invalid input! Please enter y or n: ";
//            Get input and convert it to lowercase
            cin >> input;
            lowercase_input = tolower(input);
        }
//            Ignore remaining input
        cin.ignore(INT_MAX, '\n');

//        Exit if user typed n
        if (lowercase_input == 'n') {
            break;
//        Continue if user typed y
        } else if (lowercase_input == 'y') { ;
        } else {
            cout << "\nCongratulations! I have no idea how you ended up here :o\n";
        }
    }
    return 0;
}

double fahrenheit_to_celsius(double fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
}
