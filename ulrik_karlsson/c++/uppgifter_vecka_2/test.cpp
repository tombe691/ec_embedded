#include <iostream>
#include <cmath>
using namespace std;

int main() {
  string str = "r";
  double dbl = stod(str);
  cout << dbl + 2 << endl;

  int x = pow(10, 3);
  if (x) {
    cout << x << endl;
  } else {
    cout << "Finns inte";
  }

  switch (x) {
  case 1:
    cout << "case 1\n" << x;
    break;

  case 2:
    cout << "case 2\n" << x;
    break;

  default:
    cout << "default\n" << x;
    break;
  }

  return 0;
}