//
// Created by ulrik on 2019-11-21.
//

#ifndef SPHERE_BOX_H
#define SPHERE_BOX_H

#include "shape.h"
#include <iostream>

class Box : public shape {
public:
//    Constructors
    Box() = default;
    Box(double height, double length, double width, std::string name,
        std::string color);

//    Methods
    void setBox(double height, double length, double width,
                std::string name, std::string color);
    void calcVolume();
    void calcArea();
    void print();
};


#endif //SPHERE_BOX_H
