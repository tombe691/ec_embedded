//
// Created by ulrik on 2019-11-21.
//

#include "Sphere.h"
// Constructor
Sphere::Sphere(double radius, std::string name, std::string color) {
    setSphere(radius, name, color);
}

void Sphere::setSphere(double radius, std::string name, std::string color) {
    m_radius = radius;
    m_name = name;
    m_color = color;
    calcVolume();
    calcArea();
}

void Sphere::calcVolume() {
    m_volume = 4 * M_PI * pow(m_radius, 3) / 3;
}

void Sphere::calcArea() {
    m_area = 4 * M_PI * pow(m_radius, 2);
}

void Sphere::print() {
    printName();
    printColor();
    printRadius();
    printVolume();
    printArea();
}

