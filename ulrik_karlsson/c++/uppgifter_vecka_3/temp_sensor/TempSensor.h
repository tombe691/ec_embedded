//
// Created by ulrik on 2019-11-18.
//

#ifndef TEMP_SENSOR_TEMPSENSOR_H
#define TEMP_SENSOR_TEMPSENSOR_H

#include <string>
#include <iostream>

class TempSensor {
private:
    static int global_id;
    int m_id;
    std::string m_model = "Default sensor";
    int m_batteryUsage;

    void init();

public:
    TempSensor() { init(); }
    TempSensor(std::string model);
    static void setGlobalId(int id){ global_id = id; }
    int getId();
    int getBatteryUsage();
    std::string getModel() { return m_model; }
    void setBatteryUsage(int batteryUsage) { m_batteryUsage = batteryUsage; }
    void print();
    void increaseBatteryUsage() { m_batteryUsage += 2; }
    void decreaseBatteryUsage();
};


#endif //TEMP_SENSOR_TEMPSENSOR_H
