//
// Created by ulrik on 2019-11-18.
//

#include "TempSensor.h"

// Set default start id for TempSensor
int TempSensor::global_id = 0;

void TempSensor::init() {
    m_id = global_id++;
    m_batteryUsage = 0;
}

TempSensor::TempSensor(std::string model) {
    init();
    m_model = model;
}

int TempSensor::getId() {
    return m_id;
}

int TempSensor::getBatteryUsage() {
    return m_batteryUsage;
}

void TempSensor::print() {
    std::cout << "ID: " << m_id << "\tModel: " << m_model << "\tBattery usage: " << m_batteryUsage;
}

void TempSensor::decreaseBatteryUsage() {
    m_batteryUsage -= 2;
    if (m_batteryUsage < 0) {
        m_batteryUsage = 0;
    }
}



