#include <stdio.h>

int main()
{
    char char1='H';
    char char2='e';
    char char3='l';
    char char4='l';
    char char5='o';
    char char6=' ';
    char char7='w';
    char char8='o';
    char char9='r';
    char char10='l';
    char char11='d';
    char char12='!';
    
    char strL[]="Hello world!";
    
    printf("Use printf to print: Hello World! \n");
    puts("Use puts to print: Hello world!");
    printf("Define individual char: %c%c%c%c%c%c%c%c%c%c%c%c\n", char1,char2,char3,
        char4,char5,char6,char7,char8,char9,char10,char11,char12);
    printf("Define arrary of char: %s\n", strL);
    

    return 0;
}
