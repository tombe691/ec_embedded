/* This code is made for the exercise 4.2 in program task 4.9 
 * on page 90.
 * Author: Yuan Wei
 */
 
 #include<stdio.h>

int main(){
	printf("The following displays 1 to 12 and"
		 " its squire and kubic. \n");	// Print input hint. 
	
	// Print a star line to seperate the result.
	printf("*******************************************************\n");

	//Use the for loop to print result.
	for (int i=1; i<13; i++){
		printf("%10d %10d %10d \n", i, i*i, i*i*i);
	}

	return 0;
}
