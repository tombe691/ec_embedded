//
// Created by Yuan Wei on 2019-11-20.
//
#include "Shape.h"
#ifndef VOLUMECAL_CONE_H
#define VOLUMECAL_CONE_H


class Cone : public Shape {
public:

public:
    Cone() = default;
    Cone(float r, float h, std::string name);
    float area() override;
    float volume() override;
    void setRadius(float r);
    void setHeight(float h);
};


#endif //VOLUMECAL_CONE_H
