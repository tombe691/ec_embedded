
// Created by Yuan Wei on 2019-11-20.
//

#include "FunDef.h"
#include <string>
#ifndef VOLUMECAL_SHAPE_H
#define VOLUMECAL_SHAPE_H


class Shape {

public:
    float radius;
    float width;
    float length;
    float height;
    std::string name;
public:
    Shape() = default;
    virtual float area();
    virtual float volume();
    virtual void setRadius();
};



#endif //VOLUMECAL_SHAPE_H
