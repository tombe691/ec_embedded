/* Create a file to demo pointer and use pointer to change the pointed value.
*/

#include <stdio.h>

int main()
{
    int num1 = 5;
    int *ptr;
    ptr=&num1;
    
    
    printf("The number is: %d\n", num1);
    printf("The pointed value is: %d\n", *ptr);
    printf("Type and change the number to: ");
    scanf("%d", ptr);
    printf("Now he numberis: %d and pointed value is: %d\n", num1, *ptr);
    
    
    return 0;
}

