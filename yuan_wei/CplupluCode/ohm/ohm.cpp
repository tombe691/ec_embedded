/* ohm file contains the implementation for the ohm class.
 *
 * Author: Yuan Wei
 */

#include "ohm.h"

ohm::ohm(){}
ohm::ohm(float v, float i, float r){
    voltage=v;
    ampere=i;
    resistance=r;
}
void ohm::setV(float v) {
    voltage=v;
}

void ohm::setI(float i) {
    ampere=i;
}

void ohm::setR(float r) {
    resistance=r;
}

float ohm::getV(){
    return voltage;
}
float ohm::getV(float i, float r){
    return i*r;
}

float ohm::getI(){
    return ampere;
}
float ohm::getI(float v, float r){
    return v/r;
}

float ohm::getR(){
    return resistance;
}
float ohm::getR(float v, float i){
    return v/i;
}
