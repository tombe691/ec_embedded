/* func header file contains ohm class. The fields are declared as private.
 * The default contructor and another with parameteres are used to create
 * an object automatically. The setter set the fields and getters caculate
 * and return the calcualted result.
 * 
 * Author: Yuan Wei
 */
#ifndef OHM_OHM_H
#define OHM_OHM_H


class ohm {
    private:	// The fileds are declared private to prevent direct accessing
        float voltage;
        float ampere;
        float resistance;
    public:	// The constructors and funcions are declared as public
        ohm();
        ohm(float v, float i, float r);	// Parameterized constructor

        void setV(float v);	// Setter to set voltage
        void setI(float i);	// Setter to set current
        void setR(float r);	// Setter to set resistance
        float getV();	// Getter to get voltage
        float getV(float i, float r);	// Overloaded getter given parameters

        float getI();	// Getter to get current
        float getI(float v, float r);	// Overloaded getter given parameters

        float getR();	// Getter to get resistance
        float getR(float v, float i);	// Overloaded getter given parameters
};


#endif
