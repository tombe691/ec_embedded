// Weather class implementation.
// Created by Yuan Wei on 2019-11-26.
//
#include <iostream>
#include "Weather.h"
#include <string>

using namespace std;

Weather::Weather(std::string stat, float temp, float wind) {
    this->status = stat;
    this->temperature = temp;
    this->wind = wind;
}

void Weather::setStat(std::string stat) {
    this->status = stat;
}

void Weather::setTemp(float temp) {
    this->temperature = temp;
}

void Weather::setWind(float wid) {
    this->wind = wid;
}

float Weather::fahreToCel(float f) {
    return (f - 32) * 5 / 9;
}

void Weather::showData() {
    cout << "Weather: " << status << endl;
    cout << "Temperature: " << fahreToCel(this->temperature) << " Celsius" << endl;
    cout << "Wind: " << wind << endl;
}
