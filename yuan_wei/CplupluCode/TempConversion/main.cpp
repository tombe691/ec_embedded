/*
 * Main file use Weather objects to demo guard inputs.
 * Author: Yuan Wei
 */


#include <iostream>
#include <bits/stdc++.h>
#include <string>
#include "Weather.h"

using namespace std;

int main() {
    Weather w1;
    string weather;
    float fahre;
    float wind;

    cout << "Enter the weather today(windy, rainy, cloudy or sunny): " << endl;
    cin >> weather;
    cout << "Enter the fahreheit temperature: " << endl;
    cin >> fahre;
    while (cin.fail()) {
        cin.clear(); // clear the input stream
        cin.ignore(INT_MAX, '\n'); // ignore remaining input
        cout << "Sorry only numbers!\n";
        cout << "Enter temperature in Fahrenheit: \n";
        cin >> fahre;
    }

    cout << "Enter the wind speed(km/h): " << endl;
    cin >> wind;

    w1.setStat(weather);
    w1.setTemp(fahre);
    w1.setWind(wind);

    w1.showData();


    return 0;
}