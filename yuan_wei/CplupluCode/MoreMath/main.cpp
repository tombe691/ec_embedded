#include <iostream>
#include <cmath>

using namespace std;

int choice;
double num1, num2, result;


int main()
{
    cout << "===== CALCULATOR =====\n";

    while(true) {
        cout << "1. Squre Root\n" <<
             "2. X raised to the pwoer of y \n" <<
             "3. log2\n" <<
             "4. sinX\n" <<
             "0. Exit\n\n" <<
             "Choose your operation: ";

        cin >> choice;

        switch (choice) {
            case 0:
                exit(0);
            case 1:
                cout << "Please enter number: ";
                cin >> num1;
                result = sqrt(num1);
                cout << "The square root of " << num1 << ": " << result << endl;
                break;
            case 2:
                cout << "Please enter number X: ";
                cin >> num1;
                cout << "Please enter power y: ";
                cin >> num2;
                result = pow(num1, num2);
                cout << "The " << num1 << " raised to power" << num2 << ": " << result << endl;
                break;
            case 3:
                cout << "Please enter number: ";
                cin >> num1;
                result = log2(num1);
                cout << "The log2 of " << num1 << " is: " << result << endl;
                break;
            case 4:
                cout << "Please enter angle: ";
                cin >> num1;
                result = sin(num1);
                cout << "The sin " << num1 << " is: " << result << endl;
                break;
            default:
                cout << "Invalid operation!" << endl;
        }

    }


}