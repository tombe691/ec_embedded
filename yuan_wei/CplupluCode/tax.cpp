#include <iostream>
#include <string>
using namespace std;

/* calprice take a float parameter as the net price,
 * and integer parameter as the tax rate, and returns a float
 * as the final price.
 */
float calprice(float netprices, int taxRate)
{
    // Calculate the total price and return the result.

	return netprices*(1+(float)taxRate/100);
};

int main()
{
	float netprice;	//Declare net price and tax rate variables
	int rate, select;	// Declare tax rate and selection value in the menu
	float result;	// Declare the calculate float price

	cout << "Type 1 to calculate price, 0 to abort: ";	//Display the menu
    cin >> select;
	while(1){

		// Condition to control if user wants to proceed
		if (select != 1){
			break;
		}
		else{
			cout << "Please input the net price: ";
			cin >> netprice;

			cout << "Please input tax rate in percentage: ";
			cin >> rate;

			result = calprice(netprice, rate);

			cout << "The final price with tax is: " << result << "\n";
			}

		cout << "Type 1 to calculate price, 0 to abort: ";
		cin >> select;

		}

	return(0);
}



