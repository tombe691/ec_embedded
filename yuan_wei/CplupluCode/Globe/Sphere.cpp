// Sphere header file.
// Created by Yuan Wei on 2019-11-18.
//

#include "Sphere.h"
#define PI 3.14

Sphere::Sphere() {}
Sphere::Sphere(int r) {
    radius = r;
}

void Sphere::setR(int r) {
    radius = r;
}

int Sphere::getR(){
    return radius;
}

float Sphere::getVol() {
    return (4/3.0)*PI*radius*radius*radius;
}

float Sphere::getAre() {
    return 4*PI*radius*radius;
}

