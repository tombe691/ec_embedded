// TempSensor class header file contains the class definitions.
// Created by Yuan Wei on 2019-11-24.
//

#include <string>
#ifndef TEMPSENSOR_TEMPSENSOR_H
#define TEMPSENSOR_TEMPSENSOR_H


class TempSensor {
public:
    static int sensor_ID;   // Declare a static sensor ID to track object instances
private:
    int senID;  // Sensor ID
    std::string model;  // Sensor model name
    int batteryInfo;    // Battery information
public:
    TempSensor();   // Default constructor
    TempSensor(int senID, int batt, std::string name);   // Overloaded constructor with parameters

    void setPower(int power); // Setter to set battery
    void setID(int idNum);  // Setter to set sensor ID
    std::string getModel(); // Getter to get sensor model name
    int increasePower();    // Function to increase battery level by 2
    int decreasePower();    // Function to decrease battery level by 2
    void showData();    // Function to display fields of the object
};

#endif //TEMPSENSOR_TEMPSENSOR_H
