//  TempSensor class implementations.
// Created by Yuan Wei on 2019-11-24.
//

#include "TempSensor.h"
#include <string>
#include <iostream>
using namespace std;

// Default constructor initializing with some values
TempSensor::TempSensor(){
    this -> senID = 1001;
    this -> batteryInfo = 0;
    this -> model = "favorite model";

    sensor_ID ++;   // Step static variable to track instance object number
}

// Overloaded constructor with parameters
TempSensor::TempSensor(int senID, int batt, std::string name) {
    this -> senID = senID;
    this -> batteryInfo = batt;
    this -> model = name;

    sensor_ID ++;
}

int TempSensor::sensor_ID = 0;  // Initialize class static variable with o

// Setter to set batterInfo value
void TempSensor::setPower(int power){
    this -> batteryInfo = power;
}

// Setter to set sensor ID value
void TempSensor::setID(int idNum) {
    this ->senID = idNum;
}

// Getter to get model name
std::string TempSensor::getModel() {
    return this -> model;
}

// To increase the battery level by 2
int TempSensor::increasePower(){
    return this -> batteryInfo += 2;
}

// To increase the battery level by 2. If the battery level is
// equal and less than level, the battery is not decreased.
int TempSensor::decreasePower(){
    if (this -> batteryInfo <= 0)
        return this -> batteryInfo = 0;
    else
        return this -> batteryInfo -= 2;
}

// To display the sensor field values
void TempSensor::showData(){
    cout << "Sensor ID: " << senID << endl;
    cout << "Sensor battery Consumption: " << batteryInfo << endl;
    cout << "Sensor Model: " << model << endl;
    cout << "Total sensor number: " << sensor_ID << endl;
}