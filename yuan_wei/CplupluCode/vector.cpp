/*
 * Main file to demo vector.
 *
 * Author: Yuan Wei
 */

#include <iostream>
#include <bits/stdc++.h>

using namespace std;
int main()
{
    	vector<int> myVec1 = { 1, 2, 4, 6, 9 };
	vector<int> myVec2;
	

	// Display elements of myVec1
	cout << "The elements of pre-assigned myVec1 are: " << endl;
	for(int i=0; i < myVec1.size(); i++)
		cout << myVec1[i] << " " ;

    	cout << endl;
    
	// Pop elements for myVect2	
	for (int i = 0; i < 10; i++)
		myVec2.push_back(i);
	// Display elements of myVec2
	cout << "The elements of myVec2 are: " << endl;
	for(auto x: myVec2)	
		cout << x << " ";  
	cout << endl;
	
	// Copy elements from myVect2	
	vector<int> myVec3(myVec2.begin(), myVec2.end());
	myVec3.insert(myVec3.begin(), 100);	// Change the first element of myVect3
	
	// Display elements of myVec3	
	cout << "The elements of myVec3 are now: " << endl;
	for(auto x: myVec3)	
		cout << x << " ";
	cout << endl;
	
	// Sort and display elements of myVec3 in descending order	
	sort(myVec3.begin(), myVec3.end(), greater<int>());
	cout << "The decending sorting result is: " << endl;
	for(auto x: myVec3)	
		cout << x << " ";
  
}
