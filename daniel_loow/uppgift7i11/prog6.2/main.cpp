//**************************************************
// File: prog6.2
// Summary: calculate prize without taxes
// to prize with taxes
// Version: 2.0 /c++
// Owner: Daniel Lööw
//**************************************************
#include <iostream>
using namespace std;
//function calculate prize with taxes.
double calculateTax(double prize, float tax)
{
    return prize + prize*tax/100;
}

int main()
{   //initzializing variables
    double prize;
    float tax;
    //ask for prize and while loop verify its a int you input.
    cout << "Enter what prize item cost with out taxes: ";
    while (!(cin >> prize))
    {
        //cin.clear clear error flag on cin.
        cin.clear();
        //ignore 1000 characters and discard them from input stream.
        cin.ignore(1000, '\n');
        cout << "Invalid input. Try again.\n" << "Enter what prize item cost: "<< endl;
    }
    cout << "Enter tax in %: ";
    //ask for prize and while loop verify its a float you input.
    while (!(cin >> tax))
    {
        //cin.clear the error flag on cin.
        cin.clear();
        //ignore 1000 characters and discard them from input stream.
        cin.ignore(1000, '\n');
        cout << "Invalid input. Try again.\n" << "Enter tax in %: "<< endl;
    }
    cout << "price with taxes is: "<< calculateTax(prize, tax);
    return 0;
}