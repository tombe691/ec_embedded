//
// Created by Daniel Lööw on 2019-11-28.
//
#include "functions.h"
#include <limits.h>
#include <iostream>
#include <string>
#include "Power.h"
using namespace std;
//validating values
//string "check" takes what you want to calculate ex. "resistance"
//"current" "cos" and value is value for resistance,current or cos
bool validatingValue(string check, double value){

    if(check == "resistance" && value > 20000) {
        cout << "För högt värde resistance, försök igen:\n";
        return false;
    }
    if(check == "current" && value > 400){
        cout << "För högt värde strom, försök igen:\n";
        return false;
    }
    if(check == "cos" &&  value < 0 && value  > 1){
        cout << "För högt värde, cosinus försök igen:\n";
       return false;
    }
    return true;
}
//secure input on input.
double secureInput() {
    double value;
    cin >> value;
    while (cin.fail()) {
        cin.clear();
        cin.ignore(INT_MAX, '\n');
        cout << "fel inmatning bara siffror\n" << "ange värdet igen:";
        cin >> value;
    }
    return value;
}
//*****************Meny choices********************************************
//meny choice 1
void ohmsLaw(){
    Power calc;
    printf("Ohms lag spänningen(volt/V) betäckning U lika med Resistansen(Ohm) betäckning R \n");
    printf("gånger Strömmen(Ampere) med betäckningen I. Kort U=R*I. \n\n");
    printf("Skriv resistans R < 20 000ohm: \n ");
    calc.inputResistance();
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    cout <<"Svar:" <<Power::multiplyTwoValue(calc.GetResistance(), calc.GetCurrent()) << " volt\n";
}
//meny choice 2
void totalResistance(){
    Power res1,res2,res3;
    printf("Resistans sammankopplade i parallella kretsar är lika med 1 delat Resistans R total är lika med\n");
    printf("Resistans 1/R1 + 1/R2 + 1/R3 då vi högst använder tre resistanser.\n\n");
    printf("Skriv resistans R1 < 20 000ohm: \n ");
    res1.inputResistance();
    printf("Skriv resistans R2 < 20 000ohm: \n ");
    res2.inputResistance();
    printf("Skriv resistans R3 < 20 000ohm: \n ");
    res3.inputResistance();
    cout<<"Svar:" << Power::TotalResistance(res1.GetResistance(), res2.GetResistance(), res3.GetResistance()) <<" Ohm\n";
}
//meny choice 3
void effectLaw(){
    Power calc;
    printf("Effektlagen enkel för likström är effekten P i Watt (W) lika med spänningen U i volt(V)\n");
    printf("gånger strömmen I i Ampere(A): \n\n");
    printf("Skriv spännngen U i volt(V): \n ");
    calc.SetVoltage(secureInput());
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    cout<<"Svar:" <<Power::multiplyTwoValue(calc.GetVoltage(),calc.GetCurrent()) << " Watt\n";
}
//meny choice 4
void apparentEffect(){
    Power calc;
    printf("Skenbar effekt enfas räknas med storheten VA(VoltAmpere) som är lika med spänningen U i volt(V)\n");
    printf("gånger strömmen I i ampere(A)\n\n");
    printf("Skriv Spänningen U i volt: \n ");
    calc.SetVoltage(secureInput());
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    cout<<"Svar:" <<Power::multiplyTwoValue(calc.GetVoltage(),calc.GetCurrent()) <<" VoltAmpere\n";
}
//meny choice 5
void activeAveragePower(){
    Power calc;
    printf("Aktiv medelefdekt enfas är lika med effekt P i watt(W) lika med spänningen U i volt(V) gånger strömmen I \n");
    printf("i Ampere gånger cosinus fi/efkektfaktor < 1:\n\n");
    printf("Skriv Spänningen U i volt: \n ");
    calc.SetVoltage(secureInput());
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    printf("Skriv in effektfaktorn cos > 0 && cos < 1:\n");
    calc.inputCos();
    cout<<"Svar:" << Power::ActiveAveragePower(calc.GetVoltage(),calc.GetCurrent(),calc.GetCos())<< " Watt\n";
}
//meny choice 6
void apparentEffect3Phase(){
    Power calc;
    printf("3-fas skenbar effekt är växelspänning är skenbar effekt S i voltampere(VA) lika med spänningen U i volt(V) \n");
    printf("gånger strömmen I i ampere(A) gånger roten ur 3 SQRT(3).\n\n");
    printf("Skriv Spänningen U < 400 volt: \n ");
    //exception first time in program volt not be >400 so it goes under validating in current class
    calc.inputVoltage();
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    cout<<"Svar:" << Power::ApparentEffect3Phase(calc.GetVoltage(),calc.GetCurrent()) << " Volt ampere\n";
}
//meny choice 7
void activeEffect3Phase(){
    Power calc;
    printf("3-fas aktiv effekt är effekten P i Watt(W) lika med spänningen U i volt(V) gånger strömmen I i ampere(A)\n");
    printf("gånger cos < 1 && cos > 0 gånger roten ur 3 SQRT(3).\n\n");
    printf("Skriv Spänningen U < 400 volt: \n ");
    //exception first time in program volt not be >400 so it goes under validating in current class
    calc.inputVoltage();
    printf("Skriv ström I < 440 Ampere: \n");
    calc.inputCurrent();
    printf("Skriv in effektfaktorn cos > 0 && cos < 1:\n");
    calc.inputCos();
    cout<<"Svar:" <<Power::ActiveEffect3Phase(calc.GetVoltage(),calc.GetCurrent(),calc.GetCos()) << " Watt\n";
}