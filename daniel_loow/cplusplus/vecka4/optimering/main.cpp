/*Detta program är en räknare som kan användas för ellära med enbart växelspänningar och växelströmmar. Räknaren
tar upp räkning med spänningar i volt(U), resistanser upp till 20 000/ohm(R).
Ström upp till 440 Ampere(I), effekter P i watt(W). 3 faser upp till 400V mellan faserna.
Även tar denna upp Skenbar effekt vid 3-fas och enfas, aktiv effekt vid 3-fas och enfas där cosinus fi/cosinus() används
som effektfaktorn som är mindre än 1 och inte mindre än 0.
Frekvenser i (Hz):  och totala motståndet i parallellkopplade kretsar med max 3 motstånd.

50 Hertz(Hz) Finns det i våra uttag i sverige Vid 50 Hz byter ­spänningen polaritet och strömmen riktning 100 gånger per
sekund. Spänningen i svenska eluttag pendlar upp och ner från -325 V till +325 V. Att vi talar om 230 V beror på att det
är spänningens(växelström ~) effektivvärde eller roten ur. Spänningen V(U)=Toppvärdet/sqrt(2)=325V/sqrt(2).

OHMS LAG: Spänning i volt(U)=Resistans i ohm(R)*Ström i ampere(I)
RESISTANSTOTAL PARALLELLA RESISTANSER: Rtot=1/R1R2R3
EFFEKTLAGEN ENKEL för likström: Effekt i watt(P)=U*I
SKENBAR EFFEKT ENFAS ~: Skenbar(S/VA)=U*I
AKTIV EFFEKT/MEDELEFFEKT ENFAS ~:P=U*I*cos()
SKENBAR EFFEKT 3-FAS ~: Skenbar S/VA=U*I*sqrt(3)
AKTIV EFFEKT 3-FAS ~: P=U*I*sqrt(3)*cos()
******************************************************
 File: main.cpp,functions.cpp/.h and class: Power.cpp/.h
 date: 2019-11-28
 Program for calculating electric. Ohm,resistans,current,effect.
 made some changes, code in functions and a class.
 Made by Daniel Lööw.
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "functions.h"
using namespace std;

int main()
{
    system("cls");
    bool exit = false;
   // Power calc;
    while (exit == false){
        cout <<("Välj vilka storheter du vill beräkna:\n" "Välj 1 för: OHMS LAG\n"
        "Välj 2 för: TOTAL RESISTANS\n" "Välj 3 för: EFFEKTLAGEN ENKEL\n"
        "Välj 4 för: SKENBAR EFFEKT ENFAS\n" "Välj 5 för: AKTIV EFFEKT/MEDELEFFEKT ENFAS\n"
        "Välj 6 för: SKENBAR EFFEKT 3-FAS\n" "Välj 7 för: AKTIV EFFEKT 3-FAS\n"
        "Välj 0 för: FÖR ATT AVSLUTA\n");
        int choice = secureInput();
        if(choice == 1) {
            ohmsLaw();
        }
        else if(choice == 2){
           totalResistance();
        }
        else if(choice == 3){
            effectLaw();
        }
        else if(choice == 4){
            apparentEffect();
        }
        else if(choice == 5){
            activeAveragePower();
        }
        else if(choice == 6){
            apparentEffect3Phase();
        }
        else if(choice == 7){
            activeEffect3Phase();
        }
        else if (choice == 0){
            exit = true;
        }
        else{
            printf("Fel alternativ försök igen!: \n");
        }
    }
    return 0;
}