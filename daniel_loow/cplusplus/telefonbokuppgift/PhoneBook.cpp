//
// Created by daniel on 2019-11-14.
//
#include <iostream>
#include <string>
#include "PhoneBook.h"
#include <fstream>

//city is set on orebro on default.. use this metod to change city if you are
//in different area code.
void PhoneBook::setCity (std::string cCity)
{
    city = cCity;
}
//member function show data
void PhoneBook::showData()
{
    std::cout <<"Name: " << firstName<<" "<<lastName<<" phonenumber: "<<phoneNumber
              <<" adress: "<<address<<" city: " <<city <<std::endl;
}


void PhoneBook::WriteToFile()
{
    std::ofstream outfile;
    outfile.open("Phonebook.txt",std::ios::out | std::ios::app);
    outfile << this->firstName <<" "
    << this->lastName <<" "
    << this->address << " "
    << this->city << " "
    << this->phoneNumber << std::endl;

    outfile.close();
    std::cout << "all info writen to file.";
}