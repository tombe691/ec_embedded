/*
 Mapp: telefonboksuppgift
 Files main.cpp, phonebook.cpp/.h, function.cpp/.h.
 Summary: makes a simpel phonebook adds persons to phonebook.txt.
 Date: created 2019-11-14
 Version: 1.0

 version: 1.1 update: 2019-11-15
 todo: write to file

 version: 1.2 update 2019-11-18
 write to file is complete.

 Todo: read from file, clean up code, all text in file is on same row. make meny in loop

 Owner: Daniel lööw
*/
#include <iostream>
#include <string>
#include "PhoneBook.h"
#include "function.h"

int main()
{
    meny();
    return 0;
}