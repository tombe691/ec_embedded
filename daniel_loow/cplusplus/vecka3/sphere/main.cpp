/*
 Files: main.cpp , Sphere.cpp, Sphere.h
 Created by Daniel Lööw
 Date 2019-11-18.
 Calculate program for volym and area on sphere
 Version 1.0
*/
#include <iostream>
#include <cmath>
#include "Sphere.h"

int main() {
   //initiera ett object till klassen sphere
    Sphere sphere1;
    int number_cm;
    std::cout << "Enter radius to calculate volym and area of sphere: ";
    std::cin >> number_cm;
   //while loop tar hand om fel meddelande.
    while(std::cin.fail())
    {
        //clear the input stream
        std::cin.clear();
        //ignore remaining input
        std::cin.ignore(INT_MAX, '\n');
        std::cout <<"Enter just integer\n Enter radius to calculate volym and area of sphere:";
        std::cin>> number_cm;
    }
    //sätter radien i klassen sphere
    sphere1.SetRadius(number_cm);
    //kalkylerar och skriver ut volymen och arean av klotet
    std::cout << "volume is: " << sphere1.CalculateVolume() <<" cm3" <<"\n";
    std::cout << "Area is: " << sphere1.CalculateArea() <<" cm3" <<"\n";
    return 0;
}