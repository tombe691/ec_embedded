/*
 Files: main.cpp , Sphere.cpp, Sphere.h
 Created by Daniel Lööw
 Date 2019-11-18.
 Calculate program for volym and area on sphere
 Version 1.0
*/

#ifndef SPHERE_SPHERE_H
#define SPHERE_SPHERE_H

//klassen sphere
class Sphere {
private:
    int radius;
public:
    //default constructor
    Sphere() {}
    //parameter constructor
    Sphere(int radius){
        if(radius < 0)
        {
            radius = 0;
        }
        this->radius = radius;
    }
    //för att sätta värde på radius
    void SetRadius(int radius){
        if(radius < 0)
        {
            radius = 0;
        }
        this->radius = radius;
    }
    //för att kunna se värdet på radius
    int GetRadius(){
        return this->radius;
    }
   //deklarationer för räkne metoderna
    double CalculateVolume();
    double CalculateArea();
};
#endif //SPHERE_SPHERE_H
