cmake_minimum_required(VERSION 3.15)
project(geometry)

set(CMAKE_CXX_STANDARD 17)

add_executable(geometry main.cpp Geometry.cpp Sphere.cpp Cone.cpp)