//
// Created by danie on 2019-11-20.
//

#ifndef GEOMETRY_GEOMETRY_H
#define GEOMETRY_GEOMETRY_H

//base class.
class Geometry{

public:
    double height;
    double radius;
    Geometry()= default;
    //set radius and height
    void SetRadius(double radius){
        this->radius = radius;
    }
    void SetHeight(double height){
        this->height = height;
    }
};


#endif //GEOMETRY_GEOMETRY_H
