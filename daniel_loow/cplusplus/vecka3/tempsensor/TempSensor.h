//
// Created by danie on 2019-11-20.
//

#ifndef TEMPSENSOR_TEMPSENSOR_H
#define TEMPSENSOR_TEMPSENSOR_H
#include <string>

class TempSensor{
private:
    std::string model;
    int batteryConsumption;
    //static increase id with 1 when new object is made
    static int SensorId;

public:
    //default constructor
    TempSensor(){
        this-> model = "favoritemodel";
        this-> batteryConsumption = 0;
        SensorId++;
    }
    //parameter constructor
    TempSensor(std::string model, int batteryConsumption){
        this->model = model;
        this->batteryConsumption = batteryConsumption;
        SensorId++;
    }
    //Sets and gets
    //get and sets battery consumption
    int GetBatteryConsumption(){ return this->batteryConsumption;}
    void SetBatteryConsumption(int batteryConsumption){this->batteryConsumption = batteryConsumption;}
    //increase and decrease battery consumption
    void IncreaseBatteryConsumption();
    void ReduceBatteryConsumption();
    //get model
    std::string GetModel(){return this->model;}
    //get and set sensorid
    static int GetSensorId(){return SensorId;}
    static void SetSensorId(int id){SensorId = id;}
    //printout all info of sensors
    void ShowAllInfo();
};


#endif //TEMPSENSOR_TEMPSENSOR_H
