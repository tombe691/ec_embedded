/*File: main.cpp
 * description: Tempsensor with model,id and battery consumption.
 * Created: 2019-11-18
 * By Daniel Lööw
 * Version 1.0
 *
 * change made 2019-11-20
 * version 1.5
 *
 */

#include <iostream>
#include <string>
#include "TempSensor.h"

int main()
{
    //tempsensor 1
    TempSensor temp1("Gh35", 32);
    std::cout <<"model is:"<< temp1.GetModel() <<" sensor id is:"<< temp1.GetSensorId() <<std::endl;
    //tempsensor 2
    TempSensor temp2("gg37",4);
    temp2.ShowAllInfo();
    temp2.ReduceBatteryConsumption();
    temp2.ShowAllInfo();
    //tempsensor3
    TempSensor temp3;
    temp3.ShowAllInfo();
    temp3.ReduceBatteryConsumption();
    std::cout<< "battery consumption is: " << temp3.GetBatteryConsumption() << "on temp: " <<temp3.GetSensorId();
    return 0;
}