//Owner: Elin Hildingsson
//Date: 2019-11-25

//includes the main-class (shap.h) to inherit from
#include <string>
#include "shape.h"

using namespace std;

class Box : public Shape
	 {
	 public:
	 	//variables
	 	double height;
	 	double length;
	 	double width;

	 	//constructors
	 	Box() {}
	 	Box(double height, double length, double width, string name, string colour)
	 	{
	 		this->height = height;
	 		this->length = length;
	 		this->width = width;
	 		this->name = name;
	 		this->colour = colour;
	 	}

	 	//methods
	 	double calculateArea();
	 	double calculateVolume();
	 };