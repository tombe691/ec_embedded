//Owner: Elin Hildingsson
//Date: 2019-11-25

//includes the main-class (shap.h) and the "child" class (sphere.h)
#include "sphere.h"
#include "shape.h"
#include <string>
#include <cmath>


using namespace std;

        //methods for the class sphere.h
	 	double Sphere::calculateArea()
	 	{
	 		double area;
	 		area = 4 * M_PI * pow(this->radius, 2);
			return area;
	 	}
	 	double Sphere::calculateVolume()
	 	{
	 		double volume;
	 		volume = 4 * M_PI * pow(this->radius, 3) / 3;
			return volume;
	 	}
