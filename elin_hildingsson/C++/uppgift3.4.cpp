/*File: uppgift3.4.cpp
 Summary: the program can print the user's gender and social security number if 
 	the user's social security number matches the entered gender
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-06
 -----------------------------------------------------------
 */
#include <iostream>

	using namespace std;
	int main()
	{
		//asks the user to enter 0 if the user is a boy or 1 for a girl.
		cout << "\tHello! press 0 if you are a boy and press 1 if you are a girl: \n\t";
		int boyGirl;
		cin >> boyGirl;

		//ask the user after Social Security number
		cout <<"\tplease enter your Social Security number like this YYMMDDXXXX:\n \t";
		//saves the Social Security number in s long long int
		long long int number;
		cin >> number;
		//divide with 10 to take away the last number in Social Security number
		//to to see if the answer matches the social security number or not
		number /= 10;		

		//check if the answer matches and print out correct or incorrect.
		if (boyGirl == 0 && number % 2 == 1)
	    {
	    	cout << "\tcorrect\n";
    	}

    	else if (boyGirl == 1 && number % 2 == 0)
    	{
        	cout << "\tcorrect\n";
    	}
    	else
    	{
    		cout << "\tincorrect\n";
    	}
		return 0;
	}