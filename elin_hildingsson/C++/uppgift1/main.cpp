/* File: main.cpp together with sphere.cpp and sphere.h 
 Summary: program to calculate a spheres volume and area.
 Version: 1.1
 Owner: Elin Hildingsson
 Date: 2019-11-18
 -----------------------------------------------------------
 */

#include <iostream>
//to use PI
#include <cmath>
#include "sphere.h"

	using namespace std;

	int main()
	{
		//define object.
		Sphere calculateSphere;

		//gives the radius a value.
		calculateSphere.setRadius(5);
		//variables that retrieve values ​​from the class
		double volume = calculateSphere.getVolume();
		double area = calculateSphere.getArea();
		int radius = calculateSphere.getRadius();
		cout << "\n\twhen the radius is " << radius << " the Volume of the sphere is: " << volume << 
				"\n\tand the area of the sphere is: " << area << "\n";

		//gives the radius a new value.
		calculateSphere.setRadius(-3);
		volume = calculateSphere.getVolume();
		area = calculateSphere.getArea();
		radius = calculateSphere.getRadius();
		cout << "\n\twhen the radius is " << radius << " the Volume of the sphere is: " << volume << 
				"\n\tand the area of the sphere is: " << area << "\n";
		return 0;
	}
