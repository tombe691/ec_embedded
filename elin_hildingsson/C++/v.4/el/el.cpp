//
// Created by Elin on 2019-11-28.
//
#include <cmath>
#include <iostream>
#include "menyval.h"

using namespace std;

//functions for the calculations
double multipliceraVarden(double a, double b){
    return a*b;
}

double res_tot(double r1, double r2, double r3){
    return (1/r1) + (1/r2) + (1/r3);
}

double aktiv_eff(double a, double b, double c){
    double p = a * b * c;
    return p;
}

double sken_3fas(double u, double i){
    double s = u * i * sqrt(3);
    return s;
}

double aktiv_3fas(double u, double i, double cos){
    double p = u * i * sqrt(3) * cos;
    return p;
}
//if the user enters an elevated value in menyVal you end up here.
void varning(){
    cout << "F�r h�gt v�rde, f�rs�k igen\n";
}
