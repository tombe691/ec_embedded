#include <iostream>
#include <string>

using namespace std;

class Ohm
{
    public:
    double u(double i, double r)
    {
        return i * r;
    }
    double i(double u, double r)
    {
        return u / r;
    }
    double r(double u, double i)
    {
        return u / i;
    }
};
class Effekt
{
    public:
    double p(double u, double i)
    {
        return u * i;
    }
    double u(double p, double i)
    {
        return p / i;
    }
    double i(double p, double u)
    {
        return p / u;
    }
};

int main()
{
    Ohm o1;
    Effekt e1;
    cout << "What combinations do you have?\n\n";
    cout << "Ampere and Ohm(1)" << "\nVolt and Ohm(2)" << "\nVolt and Ampere(3)"
         << "\nWatt and Ampere(4)" << "\nWatt and Volt(5)";
    cout << "\n\nEnter a number for your choice: ";
    int choice1;
    cin >> choice1;
    switch(choice1)
    {
        case 1:
            cout << "Enter Ampere: ";
            int amp1;
            cin >> amp1;
            cout << "Enter Ohm: ";
            int ohm1;
            cin >> ohm1;
            cout << "Volt = " << o1.u(amp1, ohm1) << endl;
            break;
        case 2:
            cout << "Enter Volt: ";
            int volt2;
            cin >> volt2;
            cout << "Ange Ohm: ";
            int ohm2;
            cin >> ohm2;
            cout << "Ampere = " << o1.i(volt2, ohm2) << endl;
            break;
        case 3:
            cout << "Enter Volt: ";
            int volt3;
            cin >> volt3;
            cout << "Enter Ampere: ";
            int amp3;
            cin >> amp3;
            cout << "Ohm = " << o1.r(volt3, amp3) << endl;
            cout << "Watt = " << e1.p(volt3, amp3) << endl;
            break;
        case 4:
            cout << "Enter Watt: ";
            int watt4;
            cin >> watt4;
            cout << "Enter Ampere: ";
            int amp4;
            cin >> amp4;
            cout << "Volt = " << e1.u(watt4, amp4) << endl;
            break;
        case 5:
            cout << "Enter Watt: ";
            int watt5;
            cin >> watt5;
            cout << "Enter Volt: ";
            int volt5;
            cin >> volt5;
            cout << "Ampere = " << e1.i(watt5, volt5) << endl;
            break;
        default:
            cout << "Unvalid input!";
    }
}