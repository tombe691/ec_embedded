#include <iostream>

using namespace std;

int main()
{
    int gender;
    long long int prNumber;
    cout << "Type in your gender!\n";
    cout << "0 for male and 1 for female: ";
    cin >> gender;
    cout << "Type in your personalnumber: ";
    cin >> prNumber;
    prNumber = prNumber / 10;
    if(prNumber % 2 == 1 && gender == 0 || prNumber % 2 == 0 && gender == 1)
    {
        cout << "Your gender and personalnumber is correct!\n";
    }
    else
    {
        cout << "Your gender or personalnumber is NOT correct!\n";
    }
    return 0;
}