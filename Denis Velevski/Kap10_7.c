#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void remchar(char *s, char chr)
{
   int i, j = 0;
   for ( i = 0; s[i] != '\0'; i++ )
   {
      if ( s[i] != chr )
      {
         s[j++] = s[i];
      }
   }
   s[j] = '\0';
}

int main () {
char namn [100];
printf("Filens namn?\n");
scanf("%s", namn);
FILE *infil = fopen (namn, "r");

if (infil == NULL) {
printf("Kan inte hitta filen %s", namn);
exit (1);
}
char s[100];
while (fgets (s, 100, infil) != NULL)
{

printf("Rovarsprak: %s", s); // Koden lyckas hamta Rovarsprak.txt
remchar(s, 'o');
printf("Vanligt sprak: %s", s); //Rovarsprak till vanligt sprak

}
return 0;
}
