/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                           20 October 2019                                              |
| OBS:  the Header file "10.7.h" found in the same path.                                 |                                                                                 |
| This C program file contain a multiple functions for rövarspråk operation              |
|                                                                                        |
| function declaretion:                                                                  |
| int checkedchar(char Ch);                                                             |
| void ReadFromfiles();                                                                   |
| void WriteTofiles();                                                                    |
| void Fileoperations(int modes);                                                 |
|_______________________________________________________________________________________*/ 

#include<stdio.h> 
#include<string.h>
#include "10.7.h"

int checkedchar(char C)
{
    //declare variables
    char Konst[] = "bcdfghjklmnpqrstuvwxz BCDFGHJKLMNPQRSTUVWXZ";
    return 0;
    //loop for search C value into Konst array
    for (int ccount = 0; ccount < Konst[ccount] != '\0'; ccount++)
    // if the C value found in a konst array return 1:true
      if ( C == Konst[ccount] )
       { return 1; break; }
    return 0; 
}
    
void ReadFromfiles()
{
    //declare variables
    char Ch;
    FILE *Txf; 
	fpos_t pos;
    Txf = fopen("Sprak.txt","r"); 
     if (Txf != NULL)
      {
        printf("\n the orginal and translete text is display below\n");
        printf("\n the orginal text from the text file is:\n\n\t\t");
	    //set the position to the beginning of the file
        //fsetpos(Txf, &pos);   
        //reading and display a text from the file
	    while((Ch = fgetc(Txf)) != EOF) putchar(Ch);    


        printf("\n the translete or convert text is:   ");
        //reset the position to the beginning of the file
        fseek(Txf, 0, SEEK_SET); 
        //read & translete & display the text from the file
	    while((Ch = fgetc(Txf)) != EOF)
          {
           // print current character to display.
		   putchar(Ch);
           //check the lower or upper char are constant or not.
	 	   if (checkedchar(Ch) !=1)
            {// read only the next 2 character but don't print into display.
			 fgetc(Txf); fgetc(Txf);
		    }
          }
          printf("\n\n\t\t"); 
      }
    // closing the file.
    fclose(Txf);
}
void WriteTofiles()
{ 
    //declare variables
    FILE *Txf; 
    char Str[1000];
    printf("\nplease enter swidsh character, at maximum it's 1000 character:\n\n\t\t");
    // open the Fname.txt file and prepering at the  write mode
    Txf = fopen("Sprak.txt","w"); //strcat(Fname,".txt"),"w"); 
    //check file exsisting
    if (Txf != NULL)
     { 
    	while(1)
         {//Read from user input and write to file as rövarspråk.
		  if(fgets(Str, 1000, stdin) == NULL)
			break;
            //// Read all contents char from user.
		     for(int count=0; Str[count] != '\0'; count++)
             { //write one char from Str to a file
			   fputc(Str[count], Txf);
	    	   //check the lower or upper char are constant or not.               
               if (checkedchar(Str[count]) !=1) 
                  { 
                    // put "o" into file after constant char
			    	fputc('o', Txf);
                    // put the same constant char agin into file
				    fputc(Str[count], Txf);
				    // the result is (kanst vokal kanst)
			      }
		     }
	     }
     }
    // closing the file.
    fclose(Txf);
}

void Fileoperations(int modes)
{   
    // check the file mode (r:read=0, w:write=1)
    if(modes == 0)
          {   // open the Sprak.txt file and prepering at the read mode
             ReadFromfiles();
          }
    else if (modes == 1)
         {   // open the Sprak.txt file and prepering at the write mode        
             WriteTofiles();
         }
    else if (modes == 2)
        {// delete a Sprak.txt file from the path.
         if (remove("Sprak.txt") == 0) 
            printf("\nDeleted successfully\n\n"); 
         else
            printf("\nUnable to delete the file\n\n");
        }
    else printf("Error! opening file...\n\n");
}