#include<stdio.h>
#include<math.h>
#define PI 3.14 // PI = 3.141592653589793;
/*____________________Programmeringsuppgifter 2.3 |________________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                    |
|__________________________________________________________________________________|
|Skriv en sfär. nv ett program som beräknar volymen och arean a Som indata ges     |
|färefls radie. Följande forn1ler är givna  V=(4/3)*Pi*r^3  A=  4*Pi*r^2           |
|_________________________________________________________________________________*/
void main()
{
    double Volumes,Areas,Radins;
    printf("please enter the radin");
    scanf("%lf", &Radins);
    Volumes  = 4 * PI * pow(Radins,3) / 3;
    Areas    = 4 * PI * pow(Radins,2);
    printf("\n the volume is: %0.3f",Volumes);
    printf("\n the area is: %0.3f",Areas);
}
