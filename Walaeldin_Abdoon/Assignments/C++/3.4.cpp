/*
+________________________________ Programming information 3.4 _____________________________+
|  *  File name               : 3.4.c                                                      |
|  *  Version                 : 1.2                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/06/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  : This  program that asks you if you are a guy or a girl.    |
|                               (For example, type 0 for guy and 1 for girl.) Then the     |
|                              program should ask you to enter your social security number |
|                             'without the minus sign'.scan personalized as a long long int|
|                             (The type specification is lld.) In social security numbers, |
|                              the second to last figure is odd for men and even for women.|
|                              The program should check that this is correct and print the |
|                              text correctly or incorrect. Tip: Divide by 10 to remove    |
|                              the last digit.                                             |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the check gander Library.This library is free software; you can     |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#include <iostream>
// jag bruk std Objeckt för att skriva litte kod med (cin/cout).
using namespace std;

int main()
{
    // Det här variabel för att val en person kön.
    int Person;
    // Det här variabel för att läsa och hållar Personnummber.
    long long int personnumber;
    // skriva ut ett meddelande på användarskärmen för händelse.
    cout << "Please enter the person's gender!" << endl << endl
         << "0)  Young man - (a guy) !" << endl << "1)  Young girl - (a girl) !" << endl;
    /* om I fall att användarinmatningen fel väljer programmet
     * tvinga användaren att skriva bara 0 / 1.*/
    do
        //läsa kön från tangentbord.
        cin >> Person;
    while (!( Person == 0 || Person ==1));

    // skriva ut ett meddelande på användarskärmen för händelse.
    cout << "Please enter your social security number:"<< endl;
    // läsa personnummber från tangentbord
    cin >> personnumber;

    // Dela upp personnummer med 10 för att ta bort sista siffran.
    personnumber *=  0.1;
    
    cout << "The personal numbers is " << personnumber << endl;
     /* Programmet skall kontrollera att detta
      * (personnumber mod 2) >> is 0 det betyder jämnt number.*/
        if (personnumber % 2 == 0)
             {
                if (Person == 0)
                    // skriva ut ett resultet till användarskärmen.
                   cout << "That's right with Men"<< endl;
                else
                    // skriva ut ett resultet till användarskärmen.
                   cout << "That's not true with Men"<<endl;
             }
            /* Programmet skall kontrollera att detta
             * (personnumber mod 2) >> is 1 det betyder udda number.*/
        else if (personnumber % 2 != 1)
             {
                if ( Person == 1)
                    // skriva ut ett resultet till användarskärmen.
                   cout << "That's right with women"<< endl;
                else
                    // skriva ut ett resultet till användarskärmen.
                   cout << "That's not true with women"<<endl;
             }
    return 0;
}