//
// Created by walaeldinabdoonsaee on 11/25/2019.
//
#include <iostream>
#include <list>
#include <string>
#include <cmath>
#include "Convertlib.h"

using namespace std;

void Equivalent::Num2Bin() {
    /*lldiv it's struct type have tow variables (quot, rem) as
     * "__MINGW_EXTENSION long long quot, rem" */
    lldiv_t temp;
    string Result;
    list<char> Binnumber;

    //initialize a while counter.
    temp.quot= getDec();

    // if the list container is not empty,remove all the elements.
    if (!Binnumber.empty())  Binnumber.clear();

    // convert decimal number to binary.
    while (temp.quot !=0){
        //new divided a decimal num and assigned new quot,rem into temp.
        temp = lldiv(temp.quot,2);
        /*adding a rem (0 or 1) bit digit element at the start of a list.
         * 48 is added to it according to ASCII value (0-9) and stored.
         *  temp.rem+ 48 = 0/1 + 48 >> in a character ('0'..'9') only we need ('0'..'1')*/
        Binnumber.insert(Binnumber.end(),temp.rem+48);
    }
    Result = "";

    /*Reverse the order of elements from MSB>>LSB to LSB>>MSB for right binary numbers.
     * Reverse(00101101) is Inverse the bits >> 10110100*/
    Binnumber.reverse();
   // for loop to receive binary number from list to temporary (Result)
    for(char & Ch : Binnumber) Result +=  Ch;
    Binnumber.clear();
    setBin(Result);
}
void Equivalent::Num2Oct(){
    /*lldiv it's struct type have tow variables (quot, rem) as
     * "__MINGW_EXTENSION long long quot, rem" */
    lldiv_t temp;
    string Result;
    list<char> Octnumber;

    // if the list container is not empty,remove all the elements.
    if (!Octnumber.empty())  Octnumber.clear();
    // convert Octnumber from list to decimal number.
    temp.quot = getDec();
    while (temp.quot != 0){
        temp = lldiv(temp.quot,8);
        /* add Octal digit element at the end of a list.
         * 48 is added to it according to ASCII value (0-9) and stored.
         *  temp.rem+ 48 = 0-7 + 48 >> in a character ('0'..'9') only we need ('0'..'7')*/
        Octnumber.push_back(temp.rem+48);
    }
    Result = "";
    /*Reverse the order of elements from MSB>>LSB to LSB>>MSB for right octal numbers.
     * Reverse(00163201) is Inverse the bits >> 102136100*/
    Octnumber.reverse();
    // for loop to store octal number from list to temporary (Result)
    for(char & Ch : Octnumber) Result +=  Ch;
    Octnumber.clear();
    setOct(Result);
}

void Equivalent::Num2Hex() {
    /*lldiv it's struct type have tow variables (quot, rem) as
     * "__MINGW_EXTENSION long long quot, rem" */
    lldiv_t temp;
    string Result;
    char Ch;
    list<char> hexadecimal;
    // if the list container is not empty,remove all the elements.
    if (!hexadecimal.empty())  hexadecimal.clear();
    // convert hexadecimal from list to decimal number.
    temp.quot = getDec();
    while (temp.quot != 0){
        temp = lldiv(temp.quot,16);
        // add hexadecimal digit element at the end of a list.
        if (temp.rem < 10)
            /*48 is added to it according to ASCII value (0-9) and stored.
             * temp.rem+ 48 = 0-9 + 48 >> in a character ('0'..'9') we need all alphabetic numbers*/
            hexadecimal.push_back(temp.rem + 48);
        else
            /*55 is added to it according to ASCII value (A-F) and stored.
            * temp.rem+ 55 = 0-15 + 55 >> in a character ('A'..'Z') only we need ('A'..'F')
            * temp.rem+ 87 = 0-15 + 87 >> in a character ('a'..'z') only we need ('a'..'f')*/
                hexadecimal.push_back(temp.rem + 55);
                //hexadecimal.push_back(temp.rem + 87);
    }
    Result = "";
    /*Reverse at order of elements from MSB>>LSB to LSB>>MSB for right hexadecimal numbers.
     * Reverse(001ED201) is Inverse the bits >> 1021DE100*/
    hexadecimal.reverse();
    // for loop to store hexadecimal number from list to temporary (Result)
    for(char & Ch : hexadecimal)  Result +=  Ch;
    hexadecimal.clear();
    setHex(Result);
}


void Equivalent::Bin2Num() {
    long long Result=0,counter;
    list<char> Binnumber;
    string binary = getBin();

    // if the list container is not empty,remove all the elements.
    if (!Binnumber.empty())  Binnumber.clear();

    // add binary digit element at the end of a list.
    for (counter=0;binary[counter]!='\0';counter++)
        Binnumber.push_back(binary[counter]);

    // convert binary into list to decimal number.
    //counter=0;
    for(char & Ch:Binnumber) Result += (long long)((Ch-48)*pow(2, --counter));

    // assigned Result into iDigit class member.
    setDec(Result);
}

void Equivalent::Oct2Num() {
    long long Result=0,counter;
    list<char> Octnumber;
    string Octal = getOct();

    // if the list container is not empty,remove all the elements.
    if (!Octnumber.empty())  Octnumber.clear();
    // convert Octnumber from list to decimal number.

    // add Octal digit element at the end of a list.
    for (counter=0;Octal[counter]!='\0';counter++)
        Octnumber.push_back(Octal[counter]);

    // convert Octal into list to decimal number.
    for(char & Ch:Octnumber)Result += (long long)((Ch - 48)*pow(8, --counter));

    // assigned Result into iDigit class member.
    setDec(Result);
}

void Equivalent::Hex2Num() {
    long long counter, Result=0;
    list<char> Hexnumber;
    string     Hexadecimal = getHex();

    // if the list container is not empty,remove all the elements.
    if (!Hexnumber.empty())  Hexnumber.clear();

    // add hexadecimal digit element at the end of a list.
    for (counter=0;Hexadecimal[counter]!='\0';counter++)
        Hexnumber.push_back(Hexadecimal[counter]);

    // convert hexadecimal into list to decimal number.
    for(char & Ch:Hexnumber){
        if (Ch>='0' && Ch <= '9')       Result += (long long)((Ch - 48)*pow(16, --counter));
        else if (Ch>='a' && Ch <= 'f')  Result += (long long)((Ch - 87)*pow(16, --counter));
        else if (Ch>='A' && Ch <= 'F')  Result += (long long)((Ch - 55)*pow(16, --counter));
        }

    // assigned Result into iDigit class member.
    setDec(Result);
}

/**********************************Other functions************************************/

//Define function to checked a value equal zero or not.
long long num(long long aparamter){
    if (aparamter==0)
        throw "Answer is zero for all systems......";
    return  aparamter;
}
//Define function to generate random a value.
long long  Random() {
    return (long long) rand() % (long long) (RAND_MAX);
}

void ConverttoDecimal(int Items) {
    long long Count;
    vector<string> VectHex,VectBin,VectOct; //variables for string vector type.
    list<Equivalent> Obj; // list class object.
    Equivalent Number;

    cout<<"\tConversion computer systems to other equivalent systems ..... "<<endl
        <<"\tfrom Binary, Octal and Hexadecimals to Decimal system........."<<endl
        <<"\tusing object oriented programming, vector,list and class"<< endl<< endl;

    // initialize the random number generator, convert and stored into vector.
    srand(time(NULL));
    for (Count=0; Count <= Items; Count++) {
       // Val =RANDOM(UINT32_MAX, 0);
        //long long  temp=(long long) (rand()) / ((long long) (RAND_MAX) + 1.0);

        Number.setDec(num(Random()));
        Number.From_Num_execuate();
        VectBin.insert(VectBin.end(), Number.getBin());
        VectOct.insert(VectOct.end(), Number.getOct());
        VectHex.insert(VectHex.end(), Number.getHex());
    }

    //test for inserted zero value in the last position.
    //Vectlonglong.insert(Vectlonglong.end(),0);

    cout << "Output of all elements in vector and equivalent to other systems list: " << endl << endl;

    cout<<endl<<endl<<"  ..............  vector values  .............."<<endl<<endl;
    try{
        for (Count=0; VectBin.size() > Count; Count++){

            Number.setBin(VectBin.at(Count));
            Number.setOct(VectOct.at(Count));
            Number.setHex(VectHex.at(Count));
            Number.To_Num_execuate();
            cout << "Binary in the      vector [" << Count << "] is " << Number.getBin() << endl
                 << "Octal in the       vector [" << Count << "] is " << Number.getOct() << endl
                 << "Hexadecimal in the vector [" << Count << "] is " << Number.getHex() << endl
                 << "Decimal equivalent is .............." << Number.getDec()           << endl
                 << "................................................................." <<endl;
        }
    }
    catch (const char *error){
        cerr << endl<< error << endl;
    }

    cout<<endl<<endl<<"  ..............  List Objects  .............."<<endl;

    try{
        for (Count=0; VectBin.size() > Count; Count++) {

            Number.setBin(VectBin.at(Count));
            Number.setOct(VectOct.at(Count));
            Number.setHex(VectHex.at(Count));
            Obj.insert(Obj.end(),Number);
        }

        Count=0;
        for (Equivalent obj:Obj)
        {
            obj.To_Num_execuate();

            cout << "Binary in the      object list [" << Count << "] is "  << obj.getBin() << endl
                 << "Octal in the       object list [" << Count << "] is "  << obj.getOct() << endl
                 << "Hexadecimal in the object list [" << Count << "] is "  << obj.getHex() << endl
                 << "Decimal equivalent is .............." << obj.getDec()                 << endl
                 << "................................................................."    <<endl;
            Count++;
        }
    }
    catch (const char *error){
        cerr  << endl<< error << endl;
    }

    //clean the list,vector and checked the range.
    try{
        VectBin.clear();
        VectOct.clear();
        VectHex.clear();

        VectBin.at(1);
        VectOct.at(1);
        VectHex.at(1);

        Obj.clear();
        Obj.erase(Obj.end());
    }
    catch (const out_of_range& Outofr) {
        cout << endl << "Out of Range error: " << Outofr.what() << '\n';
    }
}

void ConvertfromdDecimal(int Items) {
    long long Count;
    vector<long long> Vectlonglong; //variable for int vector type.
    list<Equivalent> Obj; // list class object.
    Equivalent Number;

    cout<<"\tConversion computer systems to other equivalent systems ..... "<<endl
        <<"\tfrom Decimal system to Binary, Octal and Hexadecimals..........."<<endl
        <<"\tusing object oriented programming, vector,list and class"<< endl<< endl;

    // initialize the random number generator and stored into vector.
    srand(time(NULL));
    for (Count=0; Count <= Items; Count++)
        Vectlonglong.insert(Vectlonglong.end(), num(Random()));

    //test for inserted zero value in the last position.
    //Vectlonglong.insert(Vectlonglong.end(),0);

    cout << "Output of all elements in vector and equivalent to other systems list: " << endl << endl;

    cout<<endl<<endl<<"  ..............  vector values  .............."<<endl<<endl;
    try{
        for (Count=0; Vectlonglong.size() > Count; Count++){

            Number.setDec(num(Vectlonglong.at(Count)));
            Number.From_Num_execuate();
            cout << "Decimal in the vector [" << Count << "] is " << Vectlonglong.at(Count)
                 << " it's equivalent of "                       << endl
                 << " binary is          " << Number.getBin()    << endl
                 << " octal is           " << Number.getOct()    << endl
                 << " Hexadecimal is     " << Number.getHex()    <<endl
                 << "................................................................."<<endl;
        }
    }
    catch (const char *error){
        cerr << endl<< error << endl;
    }

    cout<<endl<<endl<<"  ..............  List Objects  .............."<<endl<<endl;

    try{
        for (Count=0; Vectlonglong.size() > Count; Count++) {
            Number.setDec(num(Vectlonglong.at(Count)));
            Obj.insert(Obj.end(),Number);
        }
        //Obj.swap(Obj);
        //Obj.reverse();

        Count =0;
        for (Equivalent obj:Obj)
        {
            obj.From_Num_execuate();
            cout <<"Decimal in the object list ["   << Count << "] is "  << obj.getDec()
                 << " it's equivalent of "                       << endl
                 <<" binary is          " << obj.getBin()        <<endl
                 <<" octal is           " << obj.getOct()        <<endl
                 <<" Hexadecimal is     " << obj.getHex()        <<endl
                 <<"................................................................"<<endl;
            Count++;
        }
        cout <<endl <<endl ;
    }
    catch (const char *error){
        cerr  << endl<< error << endl;
    }

    //clean the list,vector and checked the vector range.
    try{
        Vectlonglong.clear();
        Obj.clear();
        Count= Vectlonglong.at(1);
        Obj.pop_back();
    }
    catch (const out_of_range& Outofr) {
        cout << endl << "Out of Range error: " << Outofr.what() << '\n';
    }
}
