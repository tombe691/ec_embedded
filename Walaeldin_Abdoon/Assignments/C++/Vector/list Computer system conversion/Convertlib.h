//
// Created by walaeldinabdoonsaee on 11/25/2019.
//

#ifndef CONVERTSYSTEM_CONVERTLIB_H
#define CONVERTSYSTEM_CONVERTLIB_H
#define RANDOM(U,L)  (rand() % (U - L + 1) + L)
#include <ctime>   // include for (time in srand)
#include <vector>
#include <string>




using namespace std;

class Equivalent{

protected:
    void Num2Bin();
    void Num2Hex();
    void Num2Oct();

    void Bin2Num();
    void Hex2Num();
    void Oct2Num();

public:
    void setDec(long long adigit){this->iDigit = adigit;}
    void setBin(string abin){this->sBinary = std::move(abin);}
    void setOct(string aoct){this->sOctal = std::move(aoct);}
    void setHex(string ahex){this->sHexadecimal = std::move(ahex);}

    long long getDec()   { return this->iDigit;}
    string getBin(){ return this->sBinary;}
    string getOct(){ return this->sOctal;}
    string getHex(){ return this->sHexadecimal;}

    void From_Num_execuate(){
        Num2Bin();
        Num2Oct();
        Num2Hex();
    }
    void To_Num_execuate(){
        Bin2Num();
        Oct2Num();
        Hex2Num();
    }
    //Equivalent(int digit):iDigit(digit) {}
private:
    long long iDigit;
    string    sBinary;
    string    sHexadecimal;
    string    sOctal;

};

/**********************************Other functions************************************/

//Declare function to checked a value equal zero or not.
long long num(long long aparamter);
void ConverttoDecimal(int Items);
void ConvertfromdDecimal(int Items);

#endif //CONVERTSYSTEM_CONVERTLIB_H
