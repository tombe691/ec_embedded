#include <iostream>
using namespace std;

struct TheBook{
    int      Total_Page;
    double   Price;
    string   Title;
    string   Author;
    string   Publicate_dates;
    string   Publishing_company;
};

void Inputnormal(struct TheBook book);
void Outputnormal(struct TheBook book);
void Inputpointer(struct TheBook *book);
void Outputpointer(struct TheBook *book);

int main() {
    struct TheBook bo;
    int TurnOff = 1;
    char Ch;

    for (;;) {
        cout << endl << endl << "please select the type of struct" << endl;
        cout << "for normal struct press N..." << endl;
        cout << "for pointer struct press P..." << endl;
        do
            cin >> Ch;
        while (!(toupper(Ch) == 'P' || toupper(Ch) == 'N'));

        if (toupper(Ch) == 'N')
            Inputnormal(bo);
        else if (toupper(Ch) == 'P')
            Inputpointer(&bo);

        /* if In case the user entry is incorrect pressed the program
         * force the user to try only 1 or 0. */
        cout << endl << "Press 1 to continue or 0 to exit: " << endl << endl;
        do
            cin >> TurnOff;//get the user entry and set into local variable.
        while (!(TurnOff == 1 || TurnOff == 0));
        if (TurnOff == 0) break;
    }
    return 0;
}

void Inputpointer(struct TheBook *book){
    cout <<endl<<"enter the a book title !"<<endl;
    cin>> book->Title;
    cout <<"enter the a book author !"<<endl;
    cin>> book->Author;
    cout <<"enter the a book total page !"<<endl;
    cin>> book->Total_Page;
    cout <<"enter the a book price !"<<endl;
    cin>> book->Price;
    cout <<"enter the a book publicate date !"<<endl;
    cin>> book->Publicate_dates;
    cout <<"enter the a book publishing company !"<<endl;
    cin>> book->Publishing_company;
    Outputpointer(book);
}
void Outputpointer(struct TheBook *book){
    cout <<endl<<"the a book title is " << book->Title <<endl;
    cout <<"book author is " <<book->Author <<endl;
    cout <<"book total page is " <<book->Total_Page<<endl;
    cout <<"book price is " <<book->Price<<endl;
    cout <<"book publican date is " <<book->Publicate_dates<<endl;
    cout <<"book publishing company is " <<book->Publishing_company<<endl<<endl<<endl;
}
void Inputnormal(struct TheBook book){
    cout <<endl<<"enter the a book title !"<<endl;
    cin>> book.Title;
    cout <<"enter the a book author !"<<endl;
    cin>> book.Author;
    cout <<"enter the a book total page !"<<endl;
    cin>> book.Total_Page;
    cout <<"enter the a book price !"<<endl;
    cin>> book.Price;
    cout <<"enter the a book publicate date !"<<endl;
    cin>> book.Publicate_dates;
    cout <<"enter the a book publishing company !"<<endl;
    cin>> book.Publishing_company;
    Outputnormal(book);
}
void Outputnormal(struct TheBook book){
    cout <<endl<<"the a book title is " << book.Title <<endl;
    cout <<"book author is " <<book.Author <<endl;
    cout <<"book total page is " <<book.Total_Page<<endl;
    cout <<"book price is " <<book.Price<<endl;
    cout <<"book publican date is " <<book.Publicate_dates<<endl;
    cout <<"book publishing company is " <<book.Publishing_company<<endl<<endl<<endl;
}