//
// Created by walaeldinabdoonsaee on 11/12/2019.
//
#include <string>
#include "Newclass.h"

using namespace std;

void Person::setFristname(string fname)  { Fname = fname;}
void Person::setSecondname(string sname) { Sname = sname;}

string Person::getFristname()  {return Fname ;}
string Person::getSecondname() {return Sname ;}

void Person::setAge(float fage) { Age = fage;}
void Person::setSalary(float fsalary) { Salary = fsalary;}

float Person::getAge() {return Age;}
float Person::geSalary() {return Salary;}