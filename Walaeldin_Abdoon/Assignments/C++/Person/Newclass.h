//
// Created by walaeldinabdoonsaee on 11/12/2019.
//

#ifndef CLASSTEST_NEWCLASS_H
#define CLASSTEST_NEWCLASS_H

#endif //CLASSTEST_NEWCLASS_H

#include <iostream>

using namespace std;

class Person //define a class
{
private:
    string Fname; //class data
    string Sname; //class data
    float Age;    //class data
    float Salary; //class data

public:
    void setFristname(string fname); //member function to set data
    void setSecondname(string sname); //member function to set data

    string getFristname(); //member function to display data
    string getSecondname(); //member function to display data

    void setAge(float fage); //member function to set data
    void setSalary(float fsalary); //member function to set data

    float getAge(); //member function to display data
    float geSalary(); //member function to display data

    Person(string fname,string sname,float age,float salary){
        setFristname(fname);
        setSecondname(sname);
        setAge(age);
        setSalary(salary);
    }
    ~Person(){
        setFristname("");
        setSecondname("");
        setAge(0.0);
        setSalary(0.0);
    }
};
