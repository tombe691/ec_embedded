/*
+________________________________  Programming information   ______________________________+
|  *  File name               : Operationlib.h                                             |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/17/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               simple calculator use Overloading functions                |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
 */
#ifndef SIMPLECALCULATOR_OPERATIONLIB_H
#define SIMPLECALCULATOR_OPERATIONLIB_H

#include <string>

using namespace std;

/*************functions for calculates mathematical operations data*************/

// Define functions for calculates a simple mathematical operations with int datatype.
int   Addition(int fnumi, int snumi){ return fnumi + snumi;}
int   Subtraction(int fnumi, int snumi){ return fnumi - snumi;}
int   Multiplication(int fnumi, int snumi){ return fnumi * snumi;}
div_t Division(int fnumi, int snumi){ return div(fnumi,snumi);}

// Define functions for calculates a simple mathematical operations with long int datatype.
long int Addition(long int fnumli,long int snumli){ return fnumli + snumli;}
long int Subtraction(long int fnumli,long int snumli){ return fnumli - snumli;}
long int Multiplication(long int fnumli,long int snumli){ return fnumli * snumli;}
ldiv_t   Division(long int fnumli,long int snumli){ return ldiv(fnumli,snumli);}

// Define functions for calculates a simple mathematical operations with long long datatype.

long long Addition(long long fnumll,long long snumll){ return fnumll + snumll;}
long long Subtraction(long long fnumll,long long snumll){ return fnumll- snumll;}
long long Multiplication(long long fnumll,long long snumll){ return fnumll * snumll;}
lldiv_t   Division(long long fnumll,long long snumll){ return lldiv(fnumll,snumll);}

// Define functions for calculates a simple mathematical operations with float datatype.
float Addition(float fnumf,float snumf){ return fnumf + snumf;}
float Subtraction(float fnumf,float snumf){ return fnumf - snumf;}
float Multiplication(float fnumf,float snumf){ return fnumf * snumf;}
float Division(float fnumf,float snumf){ float D=snumf; return (D != 0.0) ?  fnumf / snumf : 0.0;}

// Define functions for calculates a simple mathematical operations with double datatype.
double Addition(double fnumd, double snumd){ return fnumd + snumd;}
double Subtraction(double fnumd, double snumd){ return fnumd - snumd;}
double Multiplication(double fnumd, double snumd){ return fnumd * snumd;}
double Division(double fnumd, double snumd){ double D=snumd; return (D != 0.0) ? fnumd /snumd: 0.0;}

//Define function for operate from user.
void Opreitions(string fnum,string snum) {
    char Op;
    int quoti, remi;
    long int quotli, remli;
    long long quotll, remll;

    do {
        cout << endl << endl;
        cout << "Enter corresponding character to select operation......." << endl << endl;
        cout << "+). addition (N + N = N)................................" << endl;
        cout << "-). subtraction (N + N = N)............................." << endl;
        cout << "*). multiplication (N + N = N).........................." << endl;
        cout << "/). division (N / N = N) or (N div N is N, remainder N)." << endl << endl;
        cin >> Op;
    } while (!((Op == '+') || (Op == '-') || (Op == '*') || (Op == '/')));

    // Set new floating point setting format.
    cout.setf (ios :: fixed, ios :: floatfield);
    // Set three decimals point format.
    cout.precision (3);

    switch (Op) {
        case '+': { // Addition Operation.
            cout.width(6);
            cout << "1. Integer Operation.......................  (int)." << endl;

            cout << "\t" << right << stoi(fnum) << " + " << right << stoi(snum) << " = " <<
                 right << Addition(stoi(fnum), stoi(snum)) << endl << endl;

            cout << "2. Long Integer Operation..................  (long int)." << endl;

            cout << "\t" << right << stol(fnum) << " + " << right << stol(snum) << " = " <<
                 right << Addition(stol(fnum), stol(snum)) << endl << endl;

            cout << "3. Long Long Integer Operation.............  (long long)." << endl;

            cout << "\t" << right << stoll(fnum) << " + " << right << stoll(snum) << " = " <<
                 right << Addition(stoll(fnum), stoll(snum)) << endl << endl;

            cout << "4. Single Floating point format Operation... (float)." << endl;

            cout << "\t" << right << stof(fnum) << " + " << right << stof(snum) << " = " <<
                 right << Addition(stof(fnum), stof(snum)) << endl << endl;

            cout << "5. Double Floating point format Operation... (double)." << endl;

            cout << "\t" << right << stod(fnum) << " + " << right << stod(snum) << " = " <<
                 right << Addition(stod(fnum), stod(snum)) << endl << endl;
            break;
        }
        case '-': { // Subtraction Operation.
            cout.width(6);
            cout << "1. Integer Operation.......................  (int)." << endl;

            cout << "\t" << right << stoi(fnum) << " - " << right << stoi(snum) << " = " <<
                 right << Subtraction(stoi(fnum), stoi(snum)) << endl << endl;

            cout << "2. Long Integer Operation..................  (long int)." << endl;

            cout << "\t" << right << stol(fnum) << " - " << right << stol(snum) << " = " <<
                 right << Subtraction(stol(fnum), stol(snum)) << endl << endl;

            cout << "3. Long Long Integer Operation.............  (long long)." << endl;

            cout << "\t" << right << stoll(fnum) << " - " << right << stoll(snum) << " = " <<
                 right << Subtraction(stoll(fnum), stoll(snum)) << endl << endl;

            cout << "4. Single Floating point format Operation... (float)." << endl;

            cout << "\t" << right << stof(fnum) << " - " << right << stof(snum) << " = " <<
                 right << Subtraction(stof(fnum), stof(snum)) << endl << endl;

            cout << "5. Double Floating point format Operation... (double)." << endl;

            cout << "\t" << right << stod(fnum) << " - " << right << stod(snum) << " = " <<
                 right << Subtraction(stod(fnum), stod(snum)) << endl << endl;
            break;
        }
        case '*': { // Multiplication Operation.
            cout.width(6);
            cout << "1. Integer Operation.......................  (int)." << endl;

            cout << "\t" << right << stoi(fnum) << " * " << right << stoi(snum) << " = " <<
                 right << Multiplication(stoi(fnum), stoi(snum)) << endl << endl;

            cout << "2. Long Integer Operation..................  (long int)." << endl;

            cout << "\t" << right << stol(fnum) << " * " << right << stol(snum) << " = " <<
                 right << Multiplication(stol(fnum), stol(snum)) << endl << endl;

            cout << "3. Long Long Integer Operation.............  (long long)." << endl;

            cout << "\t" << right << stoll(fnum) << " * " << right << stoll(snum) << " = " <<
                 right << Multiplication(stoll(fnum), stoll(snum)) << endl << endl;

            cout << "4. Single Floating point format Operation... (float)." << endl;

            cout << "\t" << right << stof(fnum) << " * " << right << stof(snum) << " = " <<
                 right << Multiplication(stof(fnum), stof(snum)) << endl << endl;

            cout << "5. Double Floating point format Operation... (double)." << endl;

            cout << "\t" << right << stod(fnum) << " * " << right << stod(snum) << " = " <<
                 right << Multiplication(stod(fnum), stod(snum)) << endl << endl;
            break;
        }
        case '/': { // Division Operation.
            cout.width(6);
            cout << "1. Integer Operation.......................  (int)." << endl;

            quoti = Division(stoi(fnum), stoi(snum)).quot;
            remi = Division(stoi(fnum), stoi(snum)).rem;

            cout << "\t" << right << stoi(fnum) << " div " << right << stoi(snum) << " = " << " is "
                 << right << quoti << ", remainder is " << right << remi << endl << endl;

            cout << "2. Long Integer Operation..................  (long int)." << endl;
            quotli = Division(stol(fnum), stol(snum)).quot;
            remli = Division(stol(fnum), stol(snum)).rem;

            cout << "\t" << right << stol(fnum) << " div " << right << stol(snum) << " = " << " is "
                 << right << quotli << ", remainder is " << right << remli << endl << endl;

            cout << "3. Long Long Integer Operation.............  (long long)." << endl;
            quotll = Division(stoll(fnum), stoll(snum)).quot;
            remll = Division(stoll(fnum), stoll(snum)).rem;

            cout << "\t" << right << stoll(fnum) << " div " << right << stoll(snum) << " = " << " is "
                 << right << quotll << ", remainder is " << right << remll << endl  << endl;

            cout << "4. Single Floating point format Operation... (float)." << endl;

            cout << "\t" << right << stof(fnum) << " / " << right << stof(snum) << " = " <<
                 right << Division(stof(fnum), stof(snum)) << endl << endl;

            cout << "5. Double Floating point format Operation... (double)." << endl;

            cout << "\t" << right << stod(fnum) << " / " << right << stod(snum) << " = " <<
                 right << Division(stod(fnum), stod(snum)) << endl << endl;
            break;
        }
        default: {
            cout << "wrong, entry....!" << endl;
            break;
        }
    }
}

#endif //SIMPLECALCULATOR_OPERATIONLIB_H
