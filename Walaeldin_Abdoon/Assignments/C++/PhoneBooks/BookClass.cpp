/*
+________________________________  Programming information   ______________________________+
|  *  File name               : BookClass.cpp                                              |
|  *  Header File             : BookClass.h                                                |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/13/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program based on our class of person and have a multi data |
|                               and functions members,that in the order to input/output in |
|                               the record of phone book and save them temporarily until   |
|                               the terminates the application.                            |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/

// Preprocessor directives.
#include <iostream>
#include "BookClass.h"
using namespace std;

// ClassBook public member functions

//read ID member value from class.
int    ClassBook::getIndex(void)              { return ID; }
//read Phone member value from class.
string ClassBook::getPhone(void)      { return Phone; }
//read Email member value from class.
string ClassBook::getEmail(void)      { return Email; }
//read Facebook member value from class.
string ClassBook::getFacebook(void)   { return Facebook; }
//read Postort member value from class.
string ClassBook::getpostort(void)    { return Postort; }
//read Postnummer member value from class.
string ClassBook::getPostnummer(void) { return Postnummer; }
//read First/Last member values from class.
string ClassBook::getName(void){
    string strname;
    strname += Firstname;
    strname +=", ";
    strname += Lastname;
    return strname;
}
//write ID member data into class.
void ClassBook::setIndex(int Cid){
    ID = Cid;
}
//write First/Last name members data into class.
void ClassBook::setName(string Cfname,string Clname){
    Firstname = Cfname;
    Lastname  = Clname;
}
//write Phone member data into class.
void ClassBook::setPhone(string Cphone){
    Phone = Cphone;
}
//write Email member data into class.
void ClassBook::setEmail(string Femail){
    Email = Femail;
}
//write Facebook member data into class.
void ClassBook::setFacebook(string Cfacebook){
    Facebook = Cfacebook;
}
//write Postort member data into class.
void ClassBook::setpostort(string Cpostort){
    Postort = Cpostort;
}
//write Postnummer member data into class.
void ClassBook::setPostnummer(string Cpostnummer){
    Postnummer = Cpostnummer;
}



