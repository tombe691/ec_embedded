/*
+________________________________  Programming information   ______________________________+
|  *  File name               : BookClass.h                                                |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/13/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               program based on our class of person and have a multi data |
|                               and functions members,that in the order to input/output in |
|                               the record of phone book and save them temporarily until   |
|                               the terminates the application.                            |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/


#ifndef PHONEBOOKS_BOOKCLASS_H
#define PHONEBOOKS_BOOKCLASS_H

#endif //PHONEBOOKS_BOOKCLASS_H

// Preprocessor directives.
#include <iomanip>
using namespace std;

class ClassBook
{
private:
    /* Private members.
    Private keyword, means that no one can access
    the class members declared private, outside that class.*/

    // class data member are private.
    int ID; //class data
    string Firstname; //class data
    string Lastname; //class data
    string Phone; //class data
    string Email; //class data
    string Facebook; //class data
    string Postort; //class data
    string Postnummer; //class data
public:
    /*Public keyword, means all the class members declared under public will be
     * available to everyone. The data members and member functions
     * declared public can be accessed by other classes too. */

    // class member functions are public.

    //all reading data member functions.
    int getIndex(void); //member function to display data
    string getName(void); //member function to display data
    string getPhone(void); //member function to display data
    string getEmail(void); //member function to display data
    string getFacebook(void); //member function to display data
    string getpostort(void); //member function to display data
    string getPostnummer(void); //member function to display data

    //all writing data member functions.
    void setIndex(int Cid); //member function to set data
    void setName(string Cfname,string Clname); //member function to set data
    void setPhone( string Cphone); //member function to set data
    void setEmail(string Femail); //member function to set data
    void setFacebook(string Cfacebook); //member function to set data
    void setpostort(string Cpostort); //member function to set data
    void setPostnummer(string Cpostnummer); //member function to set data
/*
    // Define constructor function to initialize data members.
    ClassBook(int iD,string fname,string lname,string phone,
              string email, string facebook,string postort,string postnummer) {
        ID = iD;                 //set ID members data into class.
        Firstname = fname;       //set First name members data into class.
        Lastname = lname;        //set Last name members data into class.
        Phone = phone;           //set Phone member data into class.
        Email = email;           //set Email member data into class.
        Facebook = facebook;     //set Facebook member data into class.
        Postort = postort;       //set Postort member data into class.
        Postnummer = postnummer; //set Postnummer member data into class.
    }

    // Define Deconstruct function to distorted the data.
    ~ClassBook(){
          setIndex(0);
          setName("","");
          setPhone("");
          setEmail("");
          setFacebook("");
          setpostort("");
          setPostnummer("");
    }
    */
};
