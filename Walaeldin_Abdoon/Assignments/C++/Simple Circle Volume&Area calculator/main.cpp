/*
+________________________________  Programming information   ______________________________+
|  *  File name               : main.cpp                                                   |
|  *  Version                 : 1.0                                                        |
|  *  Owner                   : Walaeldin Abdoon Saeed Abdoon, WASA                        |
|  *  Created Date            : on 11/20/2019.                                             |
|  *  Revised and Modified by : by WASA for write code and modified from C to C++.         |
|  * Summary                  :                                                            |
|                               Simple Circle volume/area calculator using full class      |
|                                option                                                    |
+__________________________________________________________________________________________+                                                                                        |
|                                                                                          |
| This file is part of the calculation Library.This library is free software; you can      |
| redistribute it and/or modify it under the terms of the GNU General Public License as    |
| published by the Wasa Technology; either version 1.0, or (at your option)                |
| any later version.                                                                       |
|                                                                                          |
| This library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; |
| without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE |
| See the GNU General Public License for more details. If not, see                         |
| See <https://gitlab.com/tombe691/ec_embedded/blob/master/Walaeldin_Abdoon/>.             |
|                                                                                          |
|                    Copyright (C) 2000-2019 Wasa Technology, Inc.                         |
+__________________________________________________________________________________________+
*/
#include "SPhere.h"

int main() {
    Sphere newsphere;
    PrintSphere(newsphere);
    return 0;
}