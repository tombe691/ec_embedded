cmake_minimum_required(VERSION 3.15)
project(ohmslag)

set(CMAKE_CXX_STANDARD 14)

add_executable(ohmslag main.cpp OhmsOp.cpp OhmsOp.h)