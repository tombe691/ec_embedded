
int getrandom (int upperLimit, int lowerLimit);
void generaterarry(int upperLimit, int lowerLimit, int ASize, int points[ASize]);
int Summationresult(int ASize, int (*points));
double factorialresult(int ASize, int (*points));
void Ascendingsort(int ASize, int *points);
void Descendingsort(int ASize, int *points);
int getmaxvalue(int ASize, int *points);
int getminvalue(int ASize, int *points);
float averageresult(int ASize, int *points);
float medianresult(int ASize, int *points);
