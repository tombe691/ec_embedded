#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/*____________________Programmeringsuppgifter 4.2 |________________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                    |
|__________________________________________________________________________________|
|Skriv ett program som skriver ut en tabell för talen 1 till 12·                   |
|På varje rad skall talet, talet i kvadrat och talet i kubik skrivas ut.           |
|_________________________________________________________________________________*/
void main()
{ system("color 3");
    int Ordan,Kivdrat,Kubik;

    for (Ordan = 1; Ordan <= 12; Ordan++)
     { 
       Kivdrat = pow(Ordan,2); /*Kivdrat = Ordan*Ordan  */
       Kubik   = pow(Ordan,3); /*Kubik   = Ordan*Ordan*Ordan */
       printf("\tOrdning \x84r :%2d Kvadrat \x84r :%4d Kubik \x84r :%4d\n ",Ordan,Kivdrat,Kubik);   
     }
     printf("\t\n\n");

    Ordan =12;
    while (Ordan >=1)
     {
       
       Kivdrat = pow(Ordan,2); /*Kivdrat = Ordan*Ordan  */
       Kubik   = pow(Ordan,3); /*Kubik   = Ordan*Ordan*Ordan */
       printf("\tOrdning \x84r :%2d Kvadrat \x84r :%4d Kubik \x84r :%4d\n ",Ordan,Kivdrat,Kubik);       
       Ordan -=1;
     }
      printf("\t\n\n");

    Ordan = 1;
    do
     {
      
       Kivdrat = pow(Ordan,2); /*Kivdrat = Ordan*Ordan  */
       Kubik   = pow(Ordan,3); /*Kubik   = Ordan*Ordan*Ordan */
       printf("\tOrdning \x84r :%2d Kvadrat \x84r :%4d Kubik \x84r :%4d\n ",Ordan,Kivdrat,Kubik);     
       ++Ordan;
     } while (Ordan<=12);   
    
}