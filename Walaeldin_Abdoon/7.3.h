/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                                                                                        |
| OBS:  the Header file "7.3.h" found in the same path.                                  |
| This C header file contain a multiple prototypes functions for areas operations        |
|                                                                                        |
| function declaretion:                                                                  |
| double calccircle(double radies);                                                      | 
| double calcrectangel(double rwidth,double rheight);                                    | 
| double calctriangel(double rwidth,double rheight);                                     |
|_______________________________________________________________________________________*/ 

#define PI 3.14159265359

 double calccircle(double radies);                                                      
 double calcrectangel(double rwidth, double rheight);                                          
 double calctriangel(double  twidth, double theight);                                           