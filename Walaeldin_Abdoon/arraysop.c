/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 16 October 2019                                            |
| OBS:  the Header file "Arraysop.h" found in the same path.                             |
| This C program file contain a multiple functions for 1D and 2D array operations        |
| Function prototypes :                                                                  |
|                                                                                        |
| One dimation array function definions:                                                 |
|                                                                                        |
|   void generaterandom1D(int aSize, int a[aSize])                                       |
|   void maximumordering(int aSize )                                                     |
|   void minimumordering(int aSize )                                                     |
|   void print1D(int aSizeint, a[aSize] )                                                |
|   void pascaltrianglematrices(int rownumber)                                           |
|   void sortordering1D(int aSize)                                                       |
|   void distinct1D(int aSize)                                                           |
|                                                                                        |
| Tow dimation array function definions:                                                 |
|                                                                                        |
|   void generatermatrices(int aSize, int aFri[aSize][aSize], int aSec[aSize][aSize])    |   
|   void multiplysquarematrices(int aSize)                                               |
|   void additionsquarematrices(int aSize)                                               |
|   void subtractsquarematrices(int aSize)                                               |
|   long factorial(int acount)                                                           |
|   void printmatrices(int aSize, int aFrimat[aSize][aSize],int aSecmat[aSize][aSize],   |
|                      int aResmat[aSize][aSize])                                        |
|_______________________________________________________________________________________*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* -------- Part of One dimation array Function -------*/
int randoms ()
{
    int upperLimit=100, lowerLimit=0, Number;
    Number = ((rand() % (upperLimit - lowerLimit + 1)) + lowerLimit);
  return Number;
}

// Function to store random integer element an the 1D array 
void generaterandom1D(int aSize, int a[aSize]) 
{ 
    time_t t;//locale variable declaretion.
    srand((unsigned) time(&t));// Initailzation random function.
    // Pick all random integer elements one by one 
    for (int gcoun=0; gcoun< aSize; gcoun++)
        a[gcoun] = randoms();    
}
void print1D(int aSize, int a[aSize]) 
{ 
  for (int gcoun = 0; gcoun < aSize; gcoun++) 
       printf("%d\t",  a[gcoun]); /*print all the elements an the array*/   
printf("\n");
}
//C Function to find the maximum or the largest element present in an array.
void maximumordering(int aSize )
{ 
    int a[aSize]; //locale array variable declaretion.
    int maximum,location,gcoun;//locale variables declaretion.
// calling generaterandom1D for set integer random number in an array.
    generaterandom1D(aSize, a); 
    printf("\nThe orginal elements an aray is :\n");
    print1D(aSize, a);
    maximum = a[0];//get frist element an array.
 /* Compare all the elements an array with the maximum */ 
  printf("\nAfter the ordering Procces:\n");
  for (gcoun = 0; gcoun < aSize; gcoun++) 
    if (a[gcoun] > maximum){ /*if the element an array les than maximum*/
       maximum  = a[gcoun]; /*Swapp bitween maximum and element*/
       location = gcoun + 1;  /*increase the location*/
    }
  /* print the elements an the array and maximum element and location */
  printf("\nMaximum element is present at location %d and it's value is %d.\n", location, maximum);
}

//C Function to find minimum or the smallest element in an array.
void minimumordering(int aSize )
{ 
    int a[aSize]; //locale array variable declaretion.
    int minimum,location,gcoun;//locale variables declaretion.
/* call the generaterandom1D function for set integer random number in an array.*/
    generaterandom1D(aSize, a);       
    printf("\nthe orginal elements an aray is :\n");
    print1D(aSize, a);
    minimum = a[0];//get frist element an array.
 /* Compare all the elements an array with the minimum */
  for (gcoun = 0; gcoun <(aSize); gcoun++) 
    if (a[gcoun] < minimum) { /*if the element an array les than minimum*/
       minimum  = a[gcoun]; /*Swapp bitween minimum and element*/
       location = gcoun + 1; /*increase the location*/
    }
  /* print the elements an the array, minimum element and location */
  printf("\nminimum element is present at location %d and it's value is %d.\n", location, minimum);
}

// Function to sort the 1D array 
void sortordering1D(int aSize) 
{ 
    int a[aSize]; //locale array variable declaretion.  
    int outercount, inercount, tempswap; //locale variables declaretion. 
/* call the generaterandom1D function for set integer random number in an array.*/     
    generaterandom1D(aSize, a);       
    printf("\nThe orginal elements an aray is :\n");
    print1D(aSize, a);
    for (outercount = 0; outercount < aSize; outercount++) { 
        for (inercount = 0; inercount < (aSize - outercount); inercount++) { 
            if (a[inercount] > a[inercount + 1]) {   
                /* Swapping */
                tempswap = a[inercount]; 
                a[inercount] = a[inercount + 1]; 
                a[inercount + 1] = tempswap; 
            } 
        } 
    } 
    printf("\nAfter the sort Procces:\n");
    print1D(aSize, a);
}
// C Function to print all distinct elements in a given 1D array 
void distinct1D(int aSize) 
{ 
    int a[aSize]; //locale array variable declaretion. 
    int repeatcount,inercount, outercount; //locale variables declaretion. 
/* call the generaterandom _1DArray function for set integer random number in an array.*/     
    generaterandom1D(aSize, a);       
    printf("\nthe orginal elements an aray is :\n");
    print1D(aSize, a);
    // Pick all elements one by one 
    printf("the distinct elements an aray is: :\n");
    repeatcount = 0;
    for (outercount=0; outercount< aSize; outercount++) 
    { 
        // Check if the picked element is already printed 
        for (inercount=0; inercount < outercount; inercount++) 
           if (a[outercount] == a[inercount]) 
               break;   
        // If not printed earlier, then print it 
        if (outercount == inercount) {
          printf("%d\t", a[outercount]); 
          repeatcount +=1;
        }
    }
    printf("\nthere is found %d repeated elements from %d total elements an array's\n",(aSize - repeatcount), aSize); 
}     


/* -------- Part of Tow dimation array or matrices Function -------*/


// Function to store random integer number an the 2D array or matrices 
void generatermatrices( int aSize, int aFri[aSize][aSize], int aSec[aSize][aSize]) 
{ 
    time_t t;//locale variable declaretion.
    srand((unsigned) time(&t));// Initailzation random function.
    for (int outercount=0; outercount < aSize; outercount++) 
       for (int inercount=0; inercount < aSize; inercount++){
             aFri[outercount][inercount] =  randoms(); // get new random integer number.
             aSec[outercount][inercount] =  randoms(); // get new random integer number.   
             }
}
    
/* C Function to print all two square matrices and result elements in 
    a given square matrices   */
void printmatrices(int aSize,int aFrimat[aSize][aSize],int aSecmat[aSize][aSize],int aResmat[aSize][aSize])
{
    int row,col;
        /*  This part for print tow matrices and aResmat. */
    printf("\nthe frist matrix is :\n");
    for (row = 0; row < aSize; row++) 
    {    /*  This part for print frist matrices. */
        for (col = 0; col < aSize; col++) 
        printf("%d\t",aFrimat[row][col]); 
        printf("\n"); 
    }      
    /*  This part for print secand matrices. */ 
    printf("\nthe secand matrix is :\n");
    for (row = 0; row < aSize; row++) { 
        for (col = 0; col < aSize; col++) 
        printf("%d\t",aSecmat[row][col]); 
        printf("\n");
    }
    /*  This part for print the results. */
    printf("\nthe result matrices is :\n");
    for (row = 0; row < aSize; row++) {
        for (col = 0; col < aSize; col++) 
        printf("%d\t",aResmat[row][col]); 
        printf("\n");         
    } 
} 
/* C Function to multiply two square matrices and stores the result 
   in aResmat[][] and then print the resultant matrix. */
void multiplysquarematrices(int aSize ) 
{
    int row, col, depth,  aResmat[aSize][aSize];//locale variables and array declaretion.
    int aFrimat[aSize][aSize], aSecmat[aSize][aSize];//locale array variables declaretion.
/* call the generaterandom1D function for set integer random number in an array.*/
    generatermatrices(aSize,aFrimat, aSecmat);    
    for (row = 0; row < aSize; row++) 
    { 
        for (col = 0; col < aSize; col++) 
        { 
            aResmat[row][col] = 0; //initial frist element an array at zero.
            for (depth = 0; depth < aSize; depth++) 
                /*stores the result in aResmat[][] */
                aResmat[row][col] += aFrimat[row][depth] *  aSecmat[depth][col]; 
        } 
    }
    printf("\nthe product of the matrices is :\n");
    printmatrices(aSize,aFrimat, aSecmat,aResmat);/*print all the results */
} 

/* C Function to addition two square matrices, i.e., compute the sum of
   two matrices and then print the resultant matrix. */
void additionsquarematrices(int aSize )
{
    int row, col, depth,  aResmat[aSize][aSize];//locale variables and array declaretion.
    int aFrimat[aSize][aSize], aSecmat[aSize][aSize];//locale array variables declaretion.
/* call the generatermatrices function for set integer random number in an array.*/  
    generatermatrices(aSize,aFrimat, aSecmat);
   for (row = 0; row < aSize; row++) 
      for (col = 0 ; col < aSize; col++) 
         aResmat[row][col] = aFrimat[row][col] + aSecmat[row][col];/*stores the result in aResmat[][] */
   
    printf("\nthe Sum of matrices is:\n");
    printmatrices(aSize,aFrimat, aSecmat,aResmat);/*print all the results */
} 

/* C Function to finds the difference between corresponding elements of
   two matrices and then print the resultant matrix. */
void subtractsquarematrices(int aSize)  
{
    int row, col, depth,  aResmat[aSize][aSize];//locale variables and array declaretion.
    int aFrimat[aSize][aSize], aSecmat[aSize][aSize];//locale array variables declaretion.   
/* call the generatermatrices function for set integer random number in an array.*/ 
    generatermatrices(aSize,aFrimat, aSecmat);
   for (row = 0; row < aSize; row++) 
      for (col = 0 ; col < aSize; col++) 
         aResmat[row][col] = aFrimat[row][col] - aSecmat[row][col];/*stores the result in aResmat[][] */
   
    printf("\nThe subtract of matrices is:\n");
    printmatrices(aSize,aFrimat, aSecmat,aResmat);/*print all the results */
} 
    
/* C Function to print Pascal triangle which you might have studied while studying
   Binomial Theorem in Mathematics.A user will enter how many numbers of rows to print.*/
long factorial(int acount)
{
   long Temp = 1; 
   for (int counts = 1; counts <= acount; counts++)
         Temp = Temp * counts; 
   return Temp;
}

void pascaltrianglematrices(int rownumber)  
{   
   printf("\nthe max row's number is %d rows on the pascal triangle matrices:\n",rownumber);
 
   for (int outcounts = 0; outcounts < rownumber; outcounts++)
    {  for (int incounts = 0; incounts < (rownumber - outcounts -2); incounts++)
         printf("\t");
      for (int counts = 0; counts <= outcounts; counts++)
         printf("%ld\t",factorial(outcounts)/(factorial(counts)*factorial(outcounts-counts)));
      printf("\n");
    }
}
 
