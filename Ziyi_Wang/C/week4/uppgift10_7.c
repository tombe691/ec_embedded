/*Embedded C Uppgift 10.7 by Ziyi Wang 2019-10-20
Program first asks user to input som text and convert the txt into
r�varspr�k and save it into a file that the user specified. Afterwards
the r�varspr�k is displayed from the file. This file is then used as
the input, where the program then convert the text back to normal
language and display it.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mystring.h"

int main(){
	system("chcp 1252");//set up the correct encoding
	system("cls");

	/*call external function open_file, where a user specified file will be created
	and opened in the "w+" mode where both writing and reading is allowed.*/
	FILE *fp = open_file("Enter the filename: ", "w+");
	fpos_t position;//declare variable position
	fgetpos(fp, &position);//get the position at beginning of the file and save it
	char kons[] = "bcdfghjklmnpqrstvwxz";
	char s[100];
	printf("Skriv vad som helst. Det sparas s�kert p� r�varspr�ket.\nTryck ctrl+z n�r du �r f�rdig.\n");
	while(1){//Read from user input and write to file as r�varspr�k.
		if(fgets(s, 100, stdin) == NULL)
			break;
		for(int i=0; s[i] != '\0'; i++){
			fputc(s[i], fp);
			char c=tolower(s[i]);
			if(strchr(kons, c) != NULL){
				fputc('o', fp);
				fputc(c, fp);
			}
		}
	}
	system("cls");
	int c;
	printf("\nDet h�r ser din skrivning ut som r�varspr�ket.\n");
	fsetpos(fp, &position);//set the position to the beginning of the file
	while((c = fgetc(fp)) != EOF)
		putchar(c);//read and print text from the created file
	printf("\nMen faktiskt menar du det att ...\n");
	//fsetpos(fp, &position);
	fseek(fp, 0, SEEK_SET);//set the position to the beginning of the file
	while((c = fgetc(fp)) != EOF){//convert & display the text from the file
		putchar(c);
		if(strchr(kons, tolower(c)) != NULL) {
			fgetc(fp); fgetc(fp);
		}
	}
	system("pause");
	fclose(fp);
	return 0;
	}