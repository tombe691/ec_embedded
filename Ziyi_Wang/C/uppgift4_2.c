//IoT Embedded C uppgift4.2 by Ziyi Wang
#include <stdio.h>

int main(){
	int tal = 1;
	printf("%-9s%-9s%-9s\n","tal", "kvadrat","kubik");
	while(tal <= 12){
		printf("%-7d| %-7d| %-7d|\n", tal, tal*tal, tal*tal*tal);
		tal++;
	}
	return 0;
}