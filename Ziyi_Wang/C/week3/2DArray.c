/*Embedded C week 3 homework 2DArray by Ziyi Wang
Program generates random integer scores ranging between 60 to 100 and writes
them into a 2D array called score[student][course], where student and course
are arbitratily predefined as 10 and 3 respectively.

Another 2D array stats[2][course] is then generated to display the mean(first
row) and median(second row) for each course.

Both results of scores and stats are then respectively displayed. 

Mean and median are calculated by external functions calMean() and findMedian()
that are included in "func.h". There also includes the comp() function that is
required for qsort() to sort the tempCourse array.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "func.h"

int main(){
	//create 2D array to hold scores for 10 students by 3 courses 
	int student = 10;
	int course = 3;
	int score[student][course];
	//create a pointer for score (unnecessary here but just for practice)
	int (*pScore)[course] = score;

	//print the header for the score table
	printf("%s\n%18s %7s %7s\n", "Scores", "course1","course2", "course3");
	//Generate random scores range between 60-100 then read
	//scores into the score array and print the score table
	srand(time(NULL));//Used for seed
	for(int i=0; i<student; i++){
		for(int j=0; j<course; j++){
			pScore[i][j] = rand()%41+60;//Input ramdomly generated scores to the array
			if(j == 0 )//to write studentID before the first column
				printf("student%2d  %7d ", i+1, pScore[i][j]);
			else if(j!=0 && j<(course-1))
				printf("%7d ", pScore[i][j]);
			else//change line after the last column
				printf("%7d\n", pScore[i][j]);
		}
	}
	
	//create 2-D array to hold mean and median for each course
	double stats[2][course];
	//generate statistical data for 2-D array "stats"
	int tempCourse[student];//1D array contains students' scores for a specitic course
	for(int j=0; j<course; j++){
		for(int i=0; i<student; i++){
			tempCourse[i] = score[i][j];//assign data from score's column to tempCourse's row
		}
			qsort(tempCourse, student, sizeof(int), comp);//sort the array
			stats[0][j] = calMean(tempCourse, student);
			stats[1][j] = findMedian(tempCourse, student);
	}
	
	//Print stats result
	printf("\n%s\n%15s %s %s \n%-8s", "Statistics", "course1", "course2", "course3", "mean");//header
	for(int i=0; i<2; i++){
		for(int j=0; j<course; j++){
			if(i==0 && j<course-1)
				printf("%-8.1f", stats[i][j]);
			else if(i==0 && j==course-1)
				printf("%-8.1f\nmedian  ", stats[i][j]);
			else if(i==1)
				printf("%-8.1f", stats[i][j]);
		}
	}
	return 0;
}