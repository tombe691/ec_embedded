/*Embedded C week 3 homework 2DArray by Ziyi Wang
file func.c
contains external functions called from 2DArray.c*/
#include "func.h"

//Function that calculates the mean of one dimensional array of integers
double calMean(int arr[], int k){
	int sum = 0;
	double mean = 0;
	for(int i=0; i<k ; i++){
		sum += arr[i];
	}
	mean = (double) sum/k;
	return mean;
}

//Function that finds the median among integer elemends of an array
double findMedian(int arr[], int k){
	if(k%2 != 0)
		return arr[(k-1)/2];
	else
		return (arr[k/2-1]+arr[k/2])/2.0;
}

//Function needed as argument passed to qsort() 
int comp(const void *a, const void *b){
	return(*(int*)a - *(int*)b);
}