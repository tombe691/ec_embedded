/* Uppgift 6.2 by Ziyi Wang 2019-11-06
 * Program calculates the total price including moms based on user input.*/

#include <iostream>
#include <iomanip>
using namespace std;

//Function that calculates moms
double momsCalc(double prisExkMoms, double momsProcent){
    return prisExkMoms * momsProcent/100;
}

//Function that calculates total price
double prisCalc(double prisExkMoms, double momsProcent){
    return prisExkMoms * (1 + momsProcent/100);
}

//Function that prints out the receipt
void printKvitto(double momsProcent, double netto, double moms, double total){
    cout << "Moms%" << setw(6) << " " << "Netto" << setw(6) << " " <<
            "Moms" << setw(6) << " " << "Total" << endl;
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    cout << momsProcent << setw(6) << " " << netto << setw(6) << " " << moms
            << setw(6) << " " <<total << endl;
}

int main() {
    double prisExkMoms, momsProcent, moms, totalpris;
    int n;
    while(1){
        cout << "Enter price excluding moms: ";
        cin >> prisExkMoms;
        cout << "Enter percentage of moms: ";
        cin >> momsProcent;
        moms = momsCalc(prisExkMoms, momsProcent);
        totalpris = prisCalc(prisExkMoms, momsProcent);
        printKvitto(momsProcent, prisExkMoms, moms, totalpris);
        cout << "Press 1 to continue, 0 to exit: ";
        cin >> n;
        if(n == 0)
            break;
    }
    return 0;
}