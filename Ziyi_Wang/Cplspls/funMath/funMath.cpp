/* Fun math exercise using pow, log, exp from <cmath> library.
 * Created by Ziyi Wang 2019-11-25
 */

#include <iostream>
#include <iomanip>
#include <cmath>

int main() {
    std::cout << "Any number n to the power of 0 is 1.\n";
    std::cout << "0 to the power of any number of n is 0. \n";
    std::cout << "What is 0 to the power of 0? Is it 0 or 1?\n";

    std::cout << "\nPress Enter to continue. Any other key to exit.\n";
    if(getchar()!='\n') {
        exit(1);
    } else {
        system("CLS");
    }

    std::cout << "If we print x^x from x=1 to x=0 with 0.1 decrement, we can see...\n";
    for(double i = 1.0; (i - 0) > 1e-6; i -= 0.1) {
        std::cout << i << "^" << i << " = ";
        std::cout << pow(i, i) << std::endl;
    }

    std::cout << "Values took a dip until 0.4 and then it increases back.\n";
    std::cout << "What is going on?\n";


    std::cout << "\nPress Enter to continue. Any other key to exit.\n";
    if(getchar()!='\n') {
        exit(1);
    } else {
        system("CLS");
    }

    std::cout << "Let's take a look at the value of x^x between 0.4 - 0.3 with 0.01 decrement.\n";
    for(double i = 0.4; (i - 0.3) > 1e-6; i -= 0.01) {
        std::cout << i << "^" << i << " = ";
        std::cout << pow(i, i) << std::endl;
    }

    std::cout << "It seems that value hits the bottom around 0.37 and bounces back.\n";
    std::cout << "Let's look these values even closer.\n";

    std::cout << "\nPress Enter to continue. Any other key to exit.\n";
    if(getchar()!='\n') {
        exit(1);
    } else {
        system("CLS");
    }

    std::cout << "x^x between 0.38 - 0.36 with 0.001 decrement.\n";
    for(double i = 0.38; (i - 0.36) > 1e-16; i -= 0.001) {
        std::cout << i << "^" << i << " = ";
        std::cout << std::setprecision(10) << pow(i, i) << std::endl;
    }

    std::cout << "Have you seen the pattern?\n";
    std::cout << "Seems like the bottom is between 0.367 - 0.368.\n";
    std::cout << "But what significance does it have?\n";

    std::cout << "\nPress Enter to continue. Any other key to exit.\n";
    if(getchar()!='\n') {
        exit(1);
    } else {
        system("CLS");
    }

    std::cout << "Now if we take inversion of 0.367, that is 1/0.367 = ";
    std::cout << std::setprecision(6) << 1/0.367;
    std::cout << "\nHmmm... this number looks familiar.\n";
    std::cout << "Yes! It is Euler's number e = 2.7182...\n";
    std::cout << "Thus we could expect log(1/0.367) ~ log(exp(1))\n";
    std::cout << "Indeed, log(1/0.367) = ";
    std::cout << log(1/0.367);
    std::cout << " (almost 1)\n";

    std::cout << "\nPress Enter to continue. Any other key to exit.\n";
    if(getchar()!='\n') {
        exit(1);
    } else {
        system("CLS");
    }

    std::cout << "Wait! But isn't it that we are talking about 0^0?\n";
    std::cout << "Sure! So now we know 0^0 is...\n";
    std::cout << "Yes, 0^0 equals to 1 instead of 0.\n";
    std::cout << "This is because x^x has a minimum at e^e.\n";
    std::cout << "We can easily calculate this minimum with built-in functions in <cmath>\n";
    std::cout << "pow(1/exp(1), 1/exp(1)) = ";
    std::cout << pow(1/exp(1), 1/exp(1));
    std::cout << "\nSo yes, e is everywhere.\n";
    std::cout << "And this is the end of fun math.\n";
    system("pause");

    return 0;
}