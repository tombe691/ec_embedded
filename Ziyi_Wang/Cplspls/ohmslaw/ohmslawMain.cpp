/* Embedded C++ assignment: Ohm's law and Joule's law
 * Summary: Ohm's law: U = I * R
 *          Joule's law: P = U * I
 * Program calculates the unknown value in the equation according to user
 * selection and input.
 * Created by Ziyi Wang 2019-11-16 */

#include <iostream>
#include "calcFunc.h"
using namespace std;

int main() {
    //declaration of variales
    char select;
    string ustr, istr, rstr, pstr;
    double u, i, r, p;
    double *uptr = &u;
    double *iptr = &i;
    double *rptr = &r;
    double *pptr = &p;
    int calcFlag;
    int *flagPtr = &calcFlag;

    do {
        do { //prompt menu for user selection
            cout << "Enter corresponding number to select calculation." << endl;
            cout << "1. Ohm's law: U = I * R " << endl;
            cout << "2. Joule's law: P = U * I " << endl;
            cin >> select; //assign user selection to variable select
        } while (!checkSelect(select)); //loops until the valid input is achieved

        switch (select) {
            case '1': //calculate Ohm's law
                cout << "Enter values for the known parameters." << endl;
                cout << "Enter 0 for the unknown value you want to calculate." << endl;
                do {
                    do { //read user input
                        cout << "U: ";
                        cin >> ustr;
                    } while (!readValue(ustr, uptr)); //check if the input is valid
                    do {
                        cout << "I: ";
                        cin >> istr;
                    } while (!readValue(istr, iptr));
                    do {
                        cout << "R: ";
                        cin >> rstr;
                    } while (!readValue(rstr, rptr));
                } while (!checkInput(u, i, r, flagPtr)); //check if the input fulfills criteria for calculation

                switch (calcFlag) {
                    case 1: { //calculate U based on I and R
                        ohmslaw newcalc; //create an object of ohmslaw class
                        newcalc.setI(i); //set values to variable
                        newcalc.setR(r);
                        cout << "According to Ohm's law, when current I = " << istr << " amp ";
                        cout << "and resistance R = " << rstr << " ohm," << endl;
                        cout << "the voltage U is " << newcalc.calcU() << " volts." << endl;
                        break; //call corresponding function for calculation
                    }
                    case 2: { //calculate I based on U and R
                        ohmslaw newcalc2;
                        newcalc2.setU(u);
                        newcalc2.setR(r);
                        cout << "According to Ohm's law, when voltage U = " << ustr << " volts ";
                        cout << "and resistance R = " << rstr << " ohm," << endl;
                        cout << "the current I is " << newcalc2.calcI() << " amp." << endl;
                        break;
                    }
                    case 3: { //calculate P based on U and I
                        ohmslaw newcalc3;
                        newcalc3.setU(u);
                        newcalc3.setI(i);
                        cout << "According to Ohm's law, when voltage U = " << ustr << " volts ";
                        cout << "and current I = " << istr << " amp," << endl;
                        cout << "the resistance R is " << newcalc3.calcR() << " ohm." << endl;
                        break;
                    }
                    default:
                        cout << "Error! Something wrong happened." << endl;
                        break;
                }
                break;

            case '2': //calculate Joule's law
                cout << "Enter values for the known parameters." << endl;
                cout << "Enter 0 for the unknown value you want to calculate." << endl;
                do {
                    do { //read user input
                        cout << "P: ";
                        cin >> pstr;
                    } while (!readValue(pstr, pptr)); //check if the input is valid
                    do {
                        cout << "U: ";
                        cin >> ustr;
                    } while (!readValue(ustr, uptr));
                    do {
                        cout << "I: ";
                        cin >> istr;
                    } while (!readValue(istr, iptr));
                } while (!checkInput(p, u, i, flagPtr));

                switch (calcFlag) {
                    case 1: { //calculate P based on U and I
                        jouleslaw newcalc1; //create an object from jouleslaw class
                        newcalc1.setU(u); //set value to variable
                        newcalc1.setI(i);
                        cout << "According to Joule's law, when voltage U = " << ustr << " volts ";
                        cout << "and current I = " << istr << " amp," << endl;
                        cout << "the power P is " << newcalc1.calcP() << " watts." << endl;
                        break; //call function for corresponding calculation
                    }
                    case 2: { //calculate U based on P and I
                        jouleslaw newcalc2;
                        newcalc2.setP(p);
                        newcalc2.setI(i);
                        cout << "According to Joule's law, when power P = " << pstr << " watts ";
                        cout << "and current I = " << istr << " amp," << endl;
                        cout << "the voltage U is " << newcalc2.calcU() << " volts." << endl;
                        break;
                    }
                    case 3: { //calculate I based on P and U
                        jouleslaw newcalc3;
                        newcalc3.setP(p);
                        newcalc3.setU(u);
                        cout << "According to Joule's law, when power P = " << pstr << " watts ";
                        cout << "and voltage U = " << ustr << " volts," << endl;
                        cout << "the current I is " << newcalc3.calcI() << " amp." << endl;
                        break;
                    }
                }
                break;
        }
        cout << "Press enter to continue. Any other key to exit." ;
    }while(getchar() == '\n');
    return 0;
}