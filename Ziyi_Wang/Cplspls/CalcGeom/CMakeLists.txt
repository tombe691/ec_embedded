cmake_minimum_required(VERSION 3.15)
project(calcGeom)

set(CMAKE_CXX_STANDARD 14)

add_executable(calcGeom calcGeomMain.cpp obj3D.cpp obj3D.h)