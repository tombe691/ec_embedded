/* File: calcGeomMain.cpp
 * Summary: This program is to practicing inheritance and polymorphism
 *          as well as using static to keep tracking overall number of
 *          objects in the base class and specific number of objects
 *          in each derived class.
 * Log: Created by Ziyi Wang 2019-11-20
 *      Added lists for each class to track number. Ziyi Wang 2019-11-22*/

#include <iostream>
#include <list>
#include "obj3D.h"

//initiate the static member variables of each class
int Obj3D::objCount;
int Cube::cubeCount;
int Sphere::sphereCount;
int Cone::coneCount;

int main() {
    std::list<Obj3D> myObj3DList; //create a list for Obj3D
    std::list<Cube> myCubeList; //create a list for Cube
    std::list<Sphere> mySphereList; //create a list for Sphere
    std::list<Cone> myConeList; //create a list for Cone

    //create an array that contains 2 objects of class type Obj3D
    Obj3D myObj[2];
    for(int i = 0; i < 2; i++){ //create Obj3D object elements
        myObj[i].setObjID(); //each time an Obj3D object created, assign an ID to it
        myObj[i].displayObjID();
        myObj3DList.push_back(myObj[i]); //push the object to the list
    }
    //Obj3D::displayObjCount(); //display total number of Obj3D objects created
    std::cout << "You have created " << myObj3DList.size() << " 3D objects." << std::endl;


    //create an array that contains 3 objects of class type Cube
    Cube myCube[3];
    for(int i = 0; i < 3; i++){ //create Cube object elements
        myCube[i].setCubeID(); //set Cube specific ID to Cube object
        myCube[i].setObjID(); //also set an Obj3D ID to Cube object
        myCubeList.push_back(myCube[i]);//push the created Cube object to the Cubelist
        myObj3DList.push_back(myCube[i]); //push object to Obj3Dlist
        myCube[i].displayCubeID();
        myCube[i].setEdge(i + 2.5); //set eEdge through inherited function
        std::cout << "Edge length: " << myCube[i].getEdge() << std::endl;
        //calculate volume and area through inherited polymorphic functions
        std::cout << "Volume: " << myCube[i].calcVolume() << std::endl;
        std::cout << "Area: " << myCube[i].calcArea() << std::endl;
    }
    Cube::displayCubeCount(); //display updated total number of Cube objects
    Obj3D::displayObjCount(); //display updated total number of Obj3D objects

    //create an array that contains 3 objects of class type Sphere
    Sphere mySphere[4];
    for(int i = 0; i < 4; i++){ //create Sphere object elements
        mySphere[i].setSphereID(); //set Sphere specific ID to Sphere object
        mySphere[i].setObjID(); //also set an Obj3D ID to Sphere object
        mySphereList.push_back(mySphere[i]); //push the Sphere object to the list
        myObj3DList.push_back(mySphere[i]); //push object to Obj3Dlist
        mySphere[i].displaySphereID();
        mySphere[i].setRadius(i + 1.5); //set radius through inherited function
        std::cout << "Radius: " << mySphere[i].getRadius() << std::endl;
        //calculate volume and area through inherited polymorphic functions
        std::cout << "Volume: " << mySphere[i].calcVolume() << std::endl;
        std::cout << "Area: " << mySphere[i].calcArea() << std::endl;
    }
    Sphere::displaySphereCount(); //display total number of sphere objects
    Sphere::displayObjCount(); //display total number of Obj3D objects
                              //static function can be accessed from derived class

    //create an array that contains 3 objects of class type Cone
    Cone myCone[3];
    for(int i = 0; i < 3; i++){ //create Cone object elements
        myCone[i].setConeID(); //set specific ID to each Cone object
        myCone[i].setObjID(); //also set Obj3D object ID to Cone object
        myConeList.push_back(myCone[i]); //push object to ConeList
        myObj3DList.push_back(myCone[i]); //push object to Obj3Dlist
        myCone[i].displayConeID();
        myCone[i].setRadius(i + 2.5); //set radius through inherited function
        myCone[i].setHeight(i + 4.5); //set height through inherited function
        std::cout << "Radius: " << myCone[i].getRadius() << std::endl;
        std::cout << "Height: " << myCone[i].getHeight() << std::endl;
        //calculate volume and area through inherited polymorphic functions
        std::cout << "Volume: " << myCone[i].calcVolume() << std::endl;
        std::cout << "Area: " << myCone[i].calcArea() << std::endl;
    }
    Cone::displayConeCount(); //display total number of Cone objects

    //call list.size() to track number of objects in the list
    std::cout << "You have created " << myObj3DList.size() << " objects." << std::endl;
    std::cout << myCubeList.size() << " of them are cubes, ";
    std::cout << mySphereList.size() << " are spheres, and ";
    std::cout << myConeList.size() << " cones." << std::endl;


    std::cout << "You have reached the end of the program." << std::endl;
    system("pause");
    return 0;
}