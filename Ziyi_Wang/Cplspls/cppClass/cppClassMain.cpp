/* Practice on class, constructor, private/public access operator
 * created by Ziyi Wang 2019-11-13 */

#include <iostream>
#include "classFunc.h"

using namespace std;

int main() {
    Property House1("villa", 1975, 124, 5.33);
    cout << "Information of the property House1" << endl;
    House1.printInfo();
    cout <<"Unit price per square meter is " << House1.getUnitPrice() << " kr" << endl;
    cout << "This property is a " << House1.getType() << endl;

    Property House2("apartment", 1994, 74, 2.3);
    House2.setPrice(2.1);
    cout << "Information of the property House2" << endl;
    House2.printInfo();

    Property House3 = House2;
    House3.setArea(77);
    House3.setPrice(2.5);
    cout << "Information of the property House3" << endl;
    House3.printInfo();
    return 0;
}