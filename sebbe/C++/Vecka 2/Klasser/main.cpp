#include <iostream>
#include "class.h"
#include <string>

using namespace std;


int main()
{
    person player1, player2;

    player1.setAge(32);
    player2.setAge(53);

    player1.setName("Gustavos");
    player2.setName("R2D2");

    player1.showData();
    player2.showData();

    return 0;
}