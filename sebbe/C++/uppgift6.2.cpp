#include <iostream>

using namespace std;

double taxPerc(double tax)
{
    return (tax/100)+1;
}

int main()
{
    double tax;
    double price;

    cout << "What is the price for the product?(in SEK): " << endl;
    cin >> price;
    cout << "Now please enter which tax rate should be applied(in % without %%sign!" << endl;
    cin >> tax;
    cout << "The tax rate you've entered is: "<< tax << endl;
    cout << "The price without taxes is: " << price << " SEK" << endl; 
    cout << "And the total of those two are: " << price*taxPerc(tax) << " SEK" << endl;
}