/*
 * Created on Wed Nov 20 2019
 * Copyright (c) 2019 Sebastian Mossberg
 * Github @ Lemorz56
 */
#include <iostream>
#include "shape.h"
#include "cone.h"
#include "sphere.h"
using namespace std;

int main() 
{
    double radius, height;
    double *rad;
    double *hei;
    rad = &radius;
    hei = &height;

    //Create object sphere and enter value
    Sphere sphere;
    cout << "Enter the radius of the sphere: ";

    cin >> *rad;
    sphere.setRadius(*rad);

    cout <<"The Volume Of the Sphere is: "<< sphere.volumeOfSphere() << " cm" << endl;

    //Create object cone and enter the value for it
    Cone cone;
    cout << "Enter the radius you want the cone to be: " << endl;
    cin >> *rad;

    cout << "Enter the height you want the cone to be: ";
    cin >> *hei;

    cone.setRadius(*rad);
    cone.setHeight(*hei);
    
    cout << "The Volume Of the Cone is: "<< " cm" << cone.volumeOfCone() << endl;

    return 0;
}