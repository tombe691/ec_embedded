/*
 * Created on Wed Nov 20 2019
 *
 * Copyright (c) 2019 Sebastian Mossberg
 */
#include "shape.h"
#include "cone.h"
#define _USE_MATH_DEFINES
//for some reason i need this to be able to make M_PI work when including cmath
#include <cmath>

double Cone::volumeOfCone()
{
    double volume = (M_PI * pow(radius, 2) * height) / 3;

    return volume;
}
