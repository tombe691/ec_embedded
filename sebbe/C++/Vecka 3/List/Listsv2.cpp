#include <iostream>
#include <list>


using namespace std;

int main()
{
    list<double> l = {1, 2, 3, 4, 5};
    list<int> l2 = {3, 5, 9, 2, 1, 7, 4, 9, 3, 6};


    l.push_back(6);
    //unlike vectors you can push front in list
    l.push_front(10);

    //you can pop front or back as well in list
    l.pop_back();
    l.pop_front();

    cout << "The First in list is: " << l.front() << endl;
    cout << "The Last in list is: " << l.back() << endl;
    cout << endl;
    cout << "The Size: " << l.size() << endl;
    cout << endl;

    //changing values with vector l[2] = 100;
    list<double>::iterator i = l.begin();
    i++;
    *i = 100;

    //stepping through in REVERSE
//    for (list<double>::reverse_iterator i = l.rbegin(); i != l.rend(); i++)
//    {
//        cout << *i << endl;
//    }

    //this reverses our list.
    //l.reverse();

    //step through and print list
    for (list<double>::iterator i = l.begin(); i != l.end(); i++)
    {
        cout << *i << endl;
    }
    cout << endl;
    //sort LIST 2 and print it out
    l2.sort();
    for (list<int>::iterator i = l2.begin(); i != l2.end(); i++)
    {
        cout << *i << endl;
    }


    return 0;
}