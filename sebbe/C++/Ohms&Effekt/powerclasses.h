// =====================================
// SEE MAIN.cpp HEADER COMMENTS
// =====================================

#include <iostream>
using namespace std;

class ohmslaw
{
public:
    float i, r, v;
    void setValues(float paramI, float paramR, float paramV);

    void missingI(float paramV, float paramR);
    void missingR(float paramI, float paramV);
    void missingV(float paramR, float paramI);
};

class effectlaw
{
public:
    float i, r, v, p;
    void setValues(float paramI, float paramR, float paramV);

    void effectMissingV(float paramI, float paramR); //(i*i) * R;
    void effectMissingR(float paramV, float paramI);
    void effectMissingI(float paramV, float paramR);
};