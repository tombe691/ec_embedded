Bra k�mpat med uppgiften!
1. Koden �r kommenterad men du beh�ver fylla p� med mer kommentarer p� alla delar. L�nga kommentarer kan med f�rdel skrivas �ver flera rader med /**/.
Skriv ocks� vem som gjort filen och n�r h�gst upp.
2. Bra namngivning med  f�rklarande variabelnamn p� de flesta variabler, men f�rs�k undvika namn som x och y, s�ger inte en utomst�ende s� mycket. Bra att du skrivit dem p� engelska.
3. Uppgiften �r b�de en test p� vad ni l�rt er men ocks� ytterligare ett tillf�lle att tr�na.
