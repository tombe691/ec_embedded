#include <stdio.h>
#include <stdlib.h>
/*---------------
Gjorde array asignment men jag klantade till det med tiden och
la för mycket tid med att lära mig sortera, först på egen hand
och sedan med qSort().
Fick stressa sista stunden så resten blev bara klantingt och oklart.
Tänkt färdigställa denna uppgift senare.
/*--------------
//Creating a function to compare to be used by qSort.
int compare(const void *a, const void *b)
{
    int x = *(int *)a;
    int y = *(int *)b;

    if (x < y)
        return -1;
    if (x > y)
        return 1;
    return 0;
}

//lite olika variabler
int sum = 0;
float avgVal = 0;
int average(){
    avgVal = sum / 20;
}


int main()
{
    // Syntax of a 2D Array: array[rows][cols]
    //Here we create the 2D array and also define the amount of ROWS and COLUMNS since it will be easier for the for loops if it changes.
    int rows = 2, cols = 10;
    int Drivers[2][10] =
        {
            {50, 23, 43, 13, 32, 29, 43, 24, 49, 34}, //First set of drivers.
            {23, 32, 17, 6, 57, 40, 21, 11, 5, 13}    //Second set of drivers.
        };

    // Prints the array unsorted:
    printf("\nUnsorted list:\n");
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%2d, ", Drivers[i][j]);
        }
        printf("\n");
    }

    // Sort the array using qsort:
    for (int j = 0; j < rows; j++)
        qsort(Drivers[j], cols, sizeof(int), compare); //använder funktionen qsort för varje ny ROW och sorterar varje column med functionen compare.

    // Prints the array sorted:
    printf("\nSorted list:\n");
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%2d, ", Drivers[i][j]);
            //adds each value it prints to the
            sum += Drivers[i][j];
            average();
        }
        printf("\n");
    }
    /*  Klantade till det lite här men han ej felsöka. Finns bättre sätt men ville testa först. */
    double testing1 = Drivers[1][5];
    double testing2 = Drivers[1][6];
    double testing3 = Drivers[2][5];
    double testing4 = Drivers[2][6];
    int count1 = (testing1 + testing2) / 2;
    int count2 = (testing3 + testing4) / 2;

    /* MEDIAN value*/
    printf("\n Average value for every student calculated together:%.2f \n", avgVal);
    printf("Median for Drivers Class 1:%.2f\n Median for Drivers Class 2:%.2f\n",count1, count2);
    /* 
        FICK inte riktigt till det med att räkna medianen. medelvärdet funkar dock.
    */

        return 0;
}