/*
File: tempsensor.cpp
Summary: logic of the tempsensor
Owner: Isa
*/

#include <iostream>
#include "tempsensor.h"


TempSensor::TempSensor(){
        idbinary = std::bitset<4>(counterSensore+9);
        ++counterSensore;
        model = "Real Model";
        consumption = 0;
}

TempSensor::TempSensor(int consumption, std::string model){
    idbinary = std::bitset<4>(counterSensore+9);
    ++counterSensore;
    this->model = model;
    this->consumption = consumption;
}

std::string TempSensor::getModel(){
    return model;
}

void TempSensor::setConsumption(int consumption){
    this->consumption = consumption;
}

void TempSensor::increaseConsumption(){
    this->consumption += 2;
}

void TempSensor::decreaseConsumption(){
    if ((consumption-2) > -1)
        this->consumption -= 2;
}

int TempSensor::getConsumption(){
    return consumption;
}

void TempSensor::printInfo(){
    std::cout << "ID: " << idbinary << std::endl;
    std::cout << "Consumption: " << consumption << std::endl;
    std::cout << "Model: " << model << std::endl;
}