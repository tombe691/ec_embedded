/*
File: main.cpp
Summary: this program will able to create many temp sensors
Owner: Isa
*/
#include <iostream> 
#include <fstream>
#include "tempsensor.h"

int TempSensor::counterSensore;
int main(){
	system("cls");
	std::cout << "Program Started, creating objects" << std::endl;
	// create with default and parameter instance
	TempSensor t1, t2(10,"TMP36GT9Z");
	std::cout << "\nDone creating and now printing them" << std::endl;
	// diffrence between them
	t1.printInfo();
	t2.printInfo();
	std::cout << "\nMaking changes" << std::endl;
	t1.setConsumption(5);
	t2.decreaseConsumption();

	std::cout << "t1 Consumption: " << t1.getConsumption() << std::endl;
	std::cout << "t2 Consumption: " << t2.getConsumption() << std::endl;
	// cant go below 0
	t2.decreaseConsumption();
	t2.decreaseConsumption();
	t2.decreaseConsumption();
	t2.decreaseConsumption();
	t2.decreaseConsumption();

	std::cout << "t2 Consumption: " << t2.getConsumption() << std::endl;

	return 0; 
} 

// in progress
/*
std::string options[8] = {"====Options====",
                        "Create New Temp: 0", "Set/Change Consumption: 1", 
                        "Set/Change Model: 2", "Increase Consumption: 3", 
                        "Decrease Consumption: 4", "Print Info: 5",
                        "Print All Temp info: 6",
						};


void menu(){
	system("cls");
	std::cout << "TEMP SENSOR" <<std::endl;

	for (long long unsigned int i = 0; i < sizeof options / sizeof options[0]; i++){
        std::cout << options[i] << std::endl;
    }
    std::cout << "\n";
}
*/
