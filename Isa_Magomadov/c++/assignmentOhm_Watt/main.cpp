/*
File: main.cpp
Summary: this program lets user to enter values and get missing values,
        by using ohm and power law.
Owner: Isa
*/
#include <iostream>
#include <string>
#include <stdlib.h>
#include "ohmandwatt.h"

std::string options[13] = {"====Options====",
                        "Exit: 0", "Set/Change Power: 1", 
                        "Set/Change Amp: 2", "Set/Change Resistor: 3", 
                        "Set/Change Voltage: 4", "Check Values: 5",
                        "Reset Values: 6",
                        "====Calculate Missing Values Options====",
                        "Get Voltage: 7", "Get Resistor: 8",
                        "Get Amperage: 9", "Get Watt: 10"};

void menu(){
    system("cls");
    std::cout << "Ohm And Watt Calculation\n";
    std::cout << "============================\n";
    std::cout << "Set missing values as 0\n\n";

    for (long long unsigned int i = 0; i < sizeof options / sizeof options[0]; i++)
    {
        std::cout << options[i] << std::endl;
    }
    std::cout << "\n";
}

void choice(int& c,OhmAndWatt& instance){

    switch(c){
        case 0:
            instance.changeStatus(false);
            break;
        case 1:
            instance.setWatt();
            break;
        case 2:
            instance.setAmp();
            break;
        case 3:
            instance.setResistor();
            break;
        case 4:
            instance.setVoltage();
            break;
        case 5:
            instance.printValues();
            break;
        case 6:
            instance.resetValues();
            break;
        case 7:
            instance.calculateVoltage();
            break;
        case 8:
            instance.calculateResistor();
            break;
        case 9:
            instance.calculateAmp();
            break;
        case 10:
            instance.calculateWatt();
            break;
        default:
            std::cout << "there is no such option\n";
            break;
    }
}


int main(){
    OhmAndWatt cal;
    int choice1;

    while (cal.getStatus()){
        menu();
        std::cin>> choice1;
        choice(choice1,cal);
        system("pause");
    }

    return 0;
}