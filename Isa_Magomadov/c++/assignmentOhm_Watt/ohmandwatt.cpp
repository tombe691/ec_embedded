/*
File: ohmandwatt.cpp
Summary: this code calculates ohms law and watt, with known var.  
Owner: Isa
*/
#include <iostream>
#include <cmath>
#include "ohmandwatt.h"
    // sets instance watt
    void OhmAndWatt::setWatt(){
        std::cout << "enter value: ";
        std::cin >> w;
    }
    // sets instance amperage
    void OhmAndWatt::setAmp(){
        std::cout << "enter value: ";
        std::cin >> i;
    }
    // sets instance resistor
    void OhmAndWatt::setResistor(){
        std::cout << "enter value: ";
        std::cin >> r;
    }
    // sets instance voltage
    void OhmAndWatt::setVoltage(){
        std::cout << "enter value: ";
        std::cin >> v;
    }
    // print instance var
    void OhmAndWatt::printValues(){
        std::cout << "Watt: " << w << std::endl;
        std::cout << "Amp: " << i << std::endl;
        std::cout << "Resistor: " << r << std::endl;
        std::cout << "Voltage: " << v << std::endl;
    }

    void OhmAndWatt::calculateWatt(){
        // checks if watt has value
        if (w != 0){
            std::cout << "Watt has value\n";
            return;
        }
        // checks if there is any way to calculate watt by using ohm and power laws
        if (v != 0 && i != 0){
            std::cout << "w = v*i\n";
            std::cout << "Watt: " << (w = v*i) << std::endl;
        } else if (r != 0){
            
            if (v != 0){
                std::cout << "w = v*(v/r)\n";
                std::cout << "Watt: " << (w = v*(v/r)) << std::endl;
            } else if (i != 0){
                std::cout << "w = (r*i)*i\n";
                std::cout << "Watt: " << (w = (i*r)*i) << std::endl;
            }
        } else
            std::cout << "No possible ways to calculate voltage\n";
    }

    void OhmAndWatt::calculateAmp(){
        // checks if amps has value
        if (i != 0){
            std::cout << "Amperage has value\n";
            return;
        }
        // checks if there is any way to calculate amps by using ohm and power laws
        if (v != 0 && r != 0){
            std::cout << "i = v/r\n";
            std::cout << "Amperage: " << (i = v/r) << std::endl;
        } else if (w != 0 && v != 0){
            std::cout << "i = w/v\n";
            std::cout << "Amperage: " << (i = w/v) << std::endl;
        } else
            std::cout << "No possible ways to calculate voltage\n";
    
    }
    void OhmAndWatt::calculateResistor(){
        // checks if resistor has value
        if ( r != 0){
            std::cout << "Resistor has a value\n";
            return;
        }
        // checks if there is any way to calculate resistor by using ohm and power laws
        if (v != 0 && i != 0){
            std::cout << "r = v/i\n";
            std::cout << "Resistor: " << (r = v/i) << std::endl;
        } else if (w != 0){

            if (i != 0){
                std::cout << "r = (w/i)/i\n";
                std::cout << "Resistor: " << (r = (w/i)/i) << std::endl;
            } else if (v != 0){
                std::cout << "r = v/(w/v)\n";
                std::cout << "Resistor: " << (r = v/(w/v)) << std::endl;
            }

        } else
            std::cout << "No possible ways to calculate voltage\n";
    
    }
    void OhmAndWatt::calculateVoltage(){
        // checks if voltage has value
        if (v != 0){
            std::cout << "Voltage has a value\n";
            return;
        }
        // checks if there is any way to calculate voltage by using ohm and power laws
        if (r != 0 && i != 0){
            std::cout << "v = r*i\n";
            std::cout << "Voltage: " << (v = r*i) << std::endl;
            return;
        } else if(w != 0){

            if (i != 0){
                std::cout << "v = w/i\n";
                std::cout << "Voltage: " << (v = w/i) << std::endl;
                return;
            } else if (r != 0){
                std::cout << "v = srqt(w/r)*r\n";
                std::cout << "Voltage: " << (v = sqrt(w/r)*r) << std::endl;
                return;
            }

        } else
            std::cout << "No possible ways to calculate voltage\n";
    
        return;
    }
    // resets var values for the instance
    void OhmAndWatt::resetValues(){
        this->w = 0;
        this->i = 0;
        this->r = 0;
        this->v = 0;
    }
    // returns if instacne calculations status
    bool OhmAndWatt::getStatus(){
        return this->status;
    }
    // changes instance calculations status
    void OhmAndWatt::changeStatus(bool status){
        this->status = status;
    }
