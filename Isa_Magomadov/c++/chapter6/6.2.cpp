/*
File: 6.2.cpp
Summary: This program adds tax(25%) to the price.
Owner: Isa
*/
#include <iostream>

double calPrice(double price, double tax){
    return (price+(price*tax));
}

void print(std::string s){
    std::cout << s;
}

void getValue(double *pd){
    std::cin >> *pd;
}

int main(){
    double value;
    print("Enter price of the item: ");
    getValue(&value);

    // adding tax to the price and print it out
    value = calPrice(value,0.25);
    print("new price is: ");
    print(std::to_string(value));

    return 0;
}