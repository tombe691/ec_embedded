#include<stdio.h>
#include<time.h>

//
void delay(int milliseconds){
	    long pause;
    clock_t now,then;
 
    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}

int main() {
	//Endless loop that print out SOS in morse code
	while (1) {
		printf(". ");
		delay(250);
		printf(". ");
		delay(250);
		printf(". ");
		delay(500);
		printf("_ ");
		delay(500);
		printf("_ ");
		delay(500);
		printf("_ ");
		delay(250);
		printf(". ");
		delay(250);
		printf(". ");
		delay(250);
		printf(". \n");
		delay(250);
	}

	return 0;
}