Bra jobbat med uppgiften!
1. Koden �r kommenterad men skriv g�rna vem som gjort filen och f�rklara lite om uppgiften h�gst upp.
Bra med kommentarer vid funktionerna. 

2. Bra namngivning, f�rklarande variabelnamn p� de flesta variabler, testa g�rna att �ven ge i och j b�ttre namn.
3. T�nk p� tomraderna. Ha inte fler �n n�dv�ndigt och aldrig mer �n 1 tomrad mellan 2 kodrader. Var konsekvent.
4. F�r att r�kna ut median, ta reda p� hur m�nga element det finns om man inte redan vet, dela p� tv�, h�mta ut detta v�rde! N�sta ut�kning �r att l�gga ihop de tv� i mitten om det �r j�mnt antal och dela p� tv�.
5. En funktion b�r bara g�ra en sak f�r att optimera m�jligheten att �teranv�nda koden, f�rs�k om m�jligt
l�gga utskriften separat, inte s�kert man vill skriva varje g�ng man r�knar ut median eller medel.
