#include <stdio.h>

//function that calculate mean value for each column in array
void meanValue(int averageValue[2][10]) {
	
	int sum = 0;
	int rows = 1;

	for (int i=0; i<2; i++) {

		for (int j=0; j<10; j++) {
			sum = sum + averageValue[i][j];
		}

		printf("\nMean value for class %d is = %d", rows++, sum/10);		
	}

}



//function that calculate median value for each column in array
void median(int medianValue[2][10]) {
	
	int n = 10;
	double median = 0;

	for (int i=0; i<2; i++) {

		for (int j=0; j<10; j++) {
			//Lyckades aldrig få till medianuträkningen :(
		}

		printf("\nMedian value for class %d is = %.2f", i+1, median);	
	}	

}


//main function that runs the program
int main () {

	//Verify int array with 2 classes test results
	int grade[2][10] = {{2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
						{4, 4, 4, 4, 4, 4, 4, 4, 4, 4}};


	meanValue(grade);
	median(grade);

	return 0;
}