/* File: fahrenheit_to_celsius.cpp
 * Summary: Program that convert fahrenheit to celsius
 * Version: 1.1
 * Owner: Malin Neuenfelt
 * Date: 2019-11-25
 * -------------------------------*/

#include <iostream>
#include <cstdbool>
using namespace std;

class Temperature {
private:
    //Declare variables
    double fahrenheit;
    double celsius;

public:
    //Default constructor
    Temperature () = default;

    double fahrenheitToCelsius (double fahrenheit) {
        double celsius;

        return celsius = (fahrenheit-32) * 5 / 9;
    }

};

int main() {
    //Declare variables
    double fahrenheit = 0;
    double celsius = 0;
    char again;

    do {
        //Ask user to enter temperature in fahrenheit
        cout << "Enter temperature in fahrenheit: ";
        cin >> fahrenheit;

        //Loop that print out error message to user if letters are used instead of number
        while (cin.fail()) {
            cin.clear();
            cin.ignore(INT_MAX, '\n');
            cout << "Error, you can only enter numbers!" << endl;
            cout << "Enter your temperature (fahrenheit) with numbers: ";
            cin >> fahrenheit;
        }

        //Call method to convert fahrenheit to celsius
        Temperature temp1;
        celsius = temp1.fahrenheitToCelsius(fahrenheit);

        //Print out celsius
        cout << fahrenheit << " Fahrenheit converts to " << celsius << " Celsius." << endl << endl;
        cout << "Would you like to convert a new temperature?" << endl;
        cout << "Enter Y/N: ";
        cin >> again;
    }
    while (again == 'Y' || again == 'y');

    cout << "Bye!";

    return 0;
}