//
// Created by malin.neuenfeldt on 2019-11-24.
//
#ifndef ASSIGNM_SPHERE_SPHERE_H
#define ASSIGNM_SPHERE_SPHERE_H

#include "Shape.h"

class Sphere : public Shape {
public:
    //Declare variable
    int radius;

    //Constructor
    Sphere() = default;

    Sphere (int radius) {
        if (radius < 0) {
            radius = 0;
        }
        this->radius = radius;
    }
    //Method that set radius to zero if number is minus
    void setRadius (int radius) {
        if (radius < 0) {
            radius = 0;
        }
        this->radius = radius;
    }
    //Method declaration
    double calculateVolume();
    double calculateArea();
};

#endif //ASSIGNM_SPHERE_SPHERE_H

