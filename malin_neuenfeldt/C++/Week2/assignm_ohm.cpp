/* File: assignm_ohm.cpp
* Summary: Program that calculate resistance, voltage, current and power.
* Version: 1.1
* Owner: Malin Neuenfelt
* Date: 2019-11-17 */


#include <iostream>
#include <string>
using namespace std;

class ohm {
public:
    //Declare variables
    double a, b;
    //Mathod that calculate resistance
    double calculateResistance(double a, double b) {
        return a/b;
    }
    //Method that calculate voltage
    double calculateVoltage(double a, double b) {
        return a*b;
    }
    //Method that calculate current
    double calculateCurrent(double a, double b) {
        return a/b;
    }
};

class power {
public:
    //Declare variables
    double a, b;
    //Method that calculate Power
    double calculatePower(double a, double b) {
        return a*b;
    }
};

int main() {
    //Define variable
    double voltage, current, resistance;
    int menuOption;
    bool loop = true;

    //Objects
    ohm num1;
    power num2;

    //Loop that runs until user choose option 0 in menu
    while (loop == true) {

        cout << "\nMenu:\n";
        cout << "0. Quit program\n";
        cout << "1. Calculate Voltage\n";
        cout << "2. Calculate Current\n";
        cout << "3. Calculate Resistance\n";
        cout << "4. Calculate Power\n";

        cout << "\nEnter number of option: ";
        cin >> menuOption;

        switch(menuOption) {
            case 0:
                loop = false;
                break;
            case 1:
                //Ask user to enter Resistance and Current
                cout << "Enter Resistance: ";
                cin >> resistance;
                cout << "Enter Current: ";
                cin >> current;

                //Call method to calculate Voltage
                cout << "\nVoltage: "
                     << num1.calculateVoltage(resistance, current) << endl;
                break;
            case 2:
                //Ask user to enter Voltage and Resistance
                cout << "Enter Voltage: ";
                cin >> voltage;
                cout << "Enter Resistance: ";
                cin >> resistance;

                //Call method to calculate Current
                cout << "\nCurrent: "
                     << num1.calculateCurrent(voltage, resistance) << endl;
                break;
            case 3:
                //Ask user to enter Voltage and Current
                cout << "Enter Voltage: ";
                cin >> voltage;
                cout << "Enter Current: ";
                cin >> current;

                //Call method to calculate Resistance
                cout << "\nResistance: "
                    << num1.calculateResistance(voltage, current) << endl;
                break;
            case 4:
                //Ask user to enter Voltage and Current
                cout << "Enter Voltage: ";
                cin >> voltage;
                cout << "Enter Current: ";
                cin >> current;

                //Call method to calculate Power
                cout << "\nPower: "
                    << num2.calculatePower(voltage, current) << endl;
                break;
            default:
                cout << "\nOption not found. Enter a number between 0-2.\n";
        }
    }
    return 0;
}