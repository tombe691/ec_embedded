#include <iostream>
#include <string>
using namespace std;

//Structure for book
struct book {
  string title;
  string author;
  int pages;
  double price;
};

int main() {
  //Declared variables for book 1
  struct book b1;
  b1.title = "Harry Potter and the Order of the Phoenix";
  b1.author = "J.K. Rowling";
  b1.pages = 766;
  b1.price = 209;
  
  //Declared variables for book 2
  struct book b2;
  b2.title = "Harry Potter and the Half-Blood Prince";
  b2.author = "J.K. Rowling";
  b2.pages = 607;
  b2.price = 216;

  //Print out book 1
  cout << "Title: " << b1.title << "\nAuthor: " << b1.author << "\nPages: "
    << b1.pages << "\nPrice: " << b1.price << endl;

  //Print out book 2
  cout << "\nTitle: " << b2.title << "\nAuthor: " << b2.author << "\nPages: "
    << b2.pages << "\nPrice: " << b2.price << endl;
}