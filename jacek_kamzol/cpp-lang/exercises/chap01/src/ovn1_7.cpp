/*  ovn1_6.c

    jacek

    my second c++ program. print my name. what is my name?

*/

#include <iostream>

int main(void) {

  // this is a program
  std::cout << "my name is bond, james bond" << std::endl
            << "my number is 007!!. don't call me, i call you." << std::endl;

  return 0;
}
