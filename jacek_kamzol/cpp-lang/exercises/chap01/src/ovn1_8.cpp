/*  ovn1_8.c

    jacek

    my second c++ program. print my name. what is my name?
    little alarm in the end. try swedish letters. done.

*/

#include <iostream>

int main(void) {

  /* this is a program
     just to make it fun we put comments */

  std::cout << "my name is bond, james bond"
            << std::endl  // line feed
            << "my number is 007!!. don't call me, i call you."
            << std::endl  // line feed
            << "hallå på dig!"
            << std::endl  // you guessed it, line feed
            << "little alarm: \a" << std::endl;  // and a litle alarm, and line feed, again

  // return null? nothing? zipp?. what a waste. of space.
  return 0;
}
