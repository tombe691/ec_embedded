/*  just_print_out.cpp

    jacek

    the function just_print_out()

*/
#include <iostream>
#include <string>

void just_print_out(std::string my_str) {

  std::cout << my_str << std::endl;
} 
