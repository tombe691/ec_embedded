/*  just-hello.cpp

    jacek

    hello world. split function in a file.

*/

#include <iostream>
#include <string>
#include "just_print_out.hpp"

int main() {

  just_print_out("hello world");

  return 0;
}
