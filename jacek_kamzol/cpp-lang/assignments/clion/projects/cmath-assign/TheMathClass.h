//
// Created by Jacek Kamzol on 2019-11-26.
//

#ifndef CMATH_ASSIGN_THEMATHCLASS_H
#define CMATH_ASSIGN_THEMATHCLASS_H


class TheMathClass {

private:
    double m_variableNumber1 = 0;
    double m_variableNumber2 = 0;
    double m_answareNumber = 0;

public:
    TheMathClass();
    ~TheMathClass(void);

    double calcThePower(double anyNumber);

    double calcTheSquareRoot(double d);

    double calcTheCubicRoot(double d);

    double calcHypotenus(double d, double d1);

    double calcFMax(double d, double d1);
};


#endif //CMATH_ASSIGN_THEMATHCLASS_H
