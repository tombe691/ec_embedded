/* prog12_1.c

   jacek

   define a struct book. initialize the struct with atleast two books from your collection.

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// create a struct describing my book collection
struct books {
  char book_name[40];
  char book_autor[40];
  int pages_count;
  double price;
}books;

int main(int argc, char* argv[argc+1]) {

  // declare the my_book struct with direct value insertion
  struct books my_books = {"Just going home", "Jim Khan", 234, 45.5};
  // declare the my_book2 struct with indirect value passing
  struct books my_books2;
  strcpy(my_books2.book_name, "Home");
  strcpy(my_books2.book_autor, "Jenifer Halm");
  my_books2.pages_count = 456;
  my_books2.price = 34.6;

  // write out the collection
  printf("first book: %s, %s, %d pages, price: $%.2f\n", my_books.book_name, my_books.book_autor, my_books.pages_count, my_books.price);
  printf("first book: %s, %s, %d pages, price: $%.2f\n", my_books2.book_name, my_books2.book_autor, my_books2.pages_count, my_books2.price);

  return EXIT_SUCCESS;
}
