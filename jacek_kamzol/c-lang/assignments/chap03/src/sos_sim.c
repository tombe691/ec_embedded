/*  sos_som.c

    jacek

    simulate a behavior of a diod blinking out the sos message.

    pseudocode:

    the way a real microcontroller program works after a user connect it to a power source.

    initiate all or the needed periferials.
    minimum periferial needed for the diod to blink is
    clock and io_pins
    initiate the clock system, timer system
    initiate the io_pin as output
    initiate while loop for infinite looping
    in the loop, sent command to io_pin to go low (0)
    command pause or delay(500), meaning delay for 500 ms
    commands

    delay(200)
    dash()
    delay(200)
    dash()
    dalay(200)
    dash()
    delay(200)
    dot()
    delay(100)
    dot()
    delay(100)
    dot()
    delay(100)
    dash()
    delay(200)
    dash()
    delay(200)
    dash()
    delay(200)

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// #include <MCUxxx.h>  // include the header file for the mcu we are using
// #include <gpio.h>    // include the header file for the gpio
// #include <mrt.h>     // include the header filr for the clocks, timers
// #inclide <uart.h>    // include the header file for the serial port

// functions declaration
void init_power(void);
void init_clocks(void);
void init_gpio(void);
void blink_dash(void);
void blink_dot(void);
void delay(int number_of_milli_seconds);

void main(void) {

  // initiate the power system of the microcontroler
  init_power();
  // initiate the clocks or timers
  init_clocks();
  // initiate the gpio
  init_gpio();

  while(1) {
    blink_dash();
    blink_dash();
    blink_dash();
    printf(" ");
    blink_dot();
    blink_dot();
    blink_dot();
    printf(" ");
    blink_dash();
    blink_dash();
    blink_dash();
    printf("\n");
  }
}

void delay(int number_of_milli_seconds) {

  int milli_seconds = number_of_milli_seconds;
  // storing start time
  clock_t start_time = clock();
  // looping untill time is achived
  while (clock() < start_time + milli_seconds);
}

void blink_dash() {

  delay(20000);
  printf("- ");
}

void blink_dot() {

  delay(10000);
  printf(". ");
}

void init_power() {

  // initialize the power mode for the microcontroller

}

void init_clocks() {

  // initialize the clocks, timers
}

void init_gpio() {

  // init gpio. one can init the gpio for the hole system, or just the
  // ones that's needed for the moment
}
