#include"area.h"

double a_tre(double a, double b){
    return a*b;
}

double a_cir(double r){
    return PI*r*r;
}

double a_trai(double x, double y){
    return (x*y)/2;
}